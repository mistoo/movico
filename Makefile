PROJ_DIR=$(shell pwd)
export GOPATH=$(HOME)/go
export GOROOT=/usr/lib/golang

all:	blog
	go build

test:	
	go test


blog:
	$(MAKE) -C example/blog

hello:
	go build example/hello.go


clean: 
	$(MAKE) -C example/blog	clean
	-rm -f example/hello hello
	-find -name \*~ | xargs -r rm

backup:
	@cd $(PROJ_DIR);                                            \
	NAME=`basename  $(PROJ_DIR)`;                               \
	ARCHDIR=$$NAME-ARCH;                                        \
	ARCHNAME=$$NAME-`date +%Y.%m.%d-%H.%M`;                     \
	mkdir -p ~/$$ARCHDIR/$$ARCHNAME/$$NAME;                    \
	cp -a . ~/$$ARCHDIR/$$ARCHNAME/$$NAME || exit 1;           \
	cd ~/$$ARCHDIR;                                            \
	tar --bzip2 -cpf $$ARCHNAME.tar.bz2 $$ARCHNAME && rm -rf $$ARCHNAME;    \
	md5sum $$ARCHNAME.tar.bz2 > $$ARCHNAME.md5;                 \
	ARCHIVE=$$HOME/$$ARCHDIR/$$ARCHNAME.tar.bz2;        \
	ARCHIVE_MD5=$$HOME/$$ARCHDIR/$$ARCHNAME.md5;        \
	if [ $(cparch)x = "1x" ]; then                              \
	        mkdir $(backupdir)/copy || true;                    \
		cp -v $$ARCHIVE $$ARCHIVE_MD5 $(backupdir);         \
		cp -v $$ARCHIVE $$ARCHIVE_MD5 $(backupdir)/copy;    \
		cd $(backupdir) || exit 1;                          \
		md5sum --check $$ARCHIVE_MD5;                       \
		cd copy || exit 1;                                  \
		md5sum --check $$ARCHIVE_MD5;                       \
	fi;                                                         \
	md5sum --check $$ARCHIVE_MD5

arch : clean backup 

