package movico

import (
	"github.com/orfjackal/gospec/src/gospec"
	. "github.com/orfjackal/gospec/src/gospec"
	//"strings"
	"net/http"
)

func newTestRequest(app *Application, id string, query string) *Request {
	r, _ := http.NewRequest("GET", "/aliases/"+id+query, nil)
	req := app.newRequest(r, "Aliases", "Show", "")

	req.muxVars["id"] = id // set manually - no gorilla mux in tests
	req.getFormat()        // call again to alter muxVars
	return req
}

func RequestSpec(c gospec.Context) {
	app := initTestApp(nil)
	al := &Aliases{}
	app.AddController(al)

	c.Specify("Request should detect format", func() {
		formats := []string{"html", "json", "xml", "xls", "doc"}

		c.Specify("it should be html by default", func() {
			req := newTestRequest(app, "1234", "")
			c.Expect(req.Format, Equals, "html")
		})

		c.Specify("it should be detected from URI", func() {
			id := "1234"
			for _, format := range formats {
				req := newTestRequest(app, id+"."+format, "")
				c.Expect(req.URL.Path+" "+req.Format, Equals, req.URL.Path+" "+format)
				c.Expect(req.muxVars["id"], Equals, id)
				c.Expect(req.muxVars["format"], Equals, format)
			}

			id = "123482913sachvch31431746t56r457"
			for _, format := range formats {
				req := newTestRequest(app, id+"."+format, "")
				c.Expect(req.URL.Path+" "+req.Format, Equals, req.URL.Path+" "+format)
				c.Expect(req.muxVars["id"], Equals, id)
				c.Expect(req.muxVars["format"], Equals, format)
			}
		})

		c.Specify("it should be detected from URL.Path", func() {
			id := "1234abvcvs34qfw237467823r5523486"
			for _, format := range formats {
				req := newTestRequest(app, id+"."+format, "?a=1.json&b=13123141%20vgcgavc")
				c.Expect(req.URL.Path+" "+req.Format, Equals, req.URL.Path+" "+format)
				c.Expect(req.muxVars["id"], Equals, id)
				c.Expect(req.muxVars["format"], Equals, format)
			}
		})
	})

	c.Specify("Format should be passed to Response", func() {
		formats := map[string]string{
			"html": "text/html",
			"txt":  "text/plain",
			"json": "application/json",
			"xml":  "application/xml",
			"xls":  "application/vnd.ms-excel",
			"doc":  "application/msword",
		}

		id := "1234"
		for format, mt := range formats {
			req := newTestRequest(app, id+"."+format, "")
			re := req.Response()
			c.Expect(req.URL.Path+" "+re.contentType, Equals, req.URL.Path+" "+mt)
		}
	})
}
