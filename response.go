package movico

import (
	"bytes"
	"errors"
	"fmt"
	"mime"
	"net/http"
	"path"
	"strconv"
	"strings"
	"sync"
)

// Implements http.ResponseWriter as a buffer
type Response struct {
	bytes.Buffer
	code        int
	headers     http.Header
	once        sync.Once
	req         *Request
	contentType string
	charset     string
}

func newResponse(req *Request) *Response {
	re := new(Response)
	re.contentType = "text/html" // default
	if req.Format != "html" {
		re.SetContentType("." + req.Format)
		//fmt.Printf("%s => %s\n", req.Format, re.contentType)
	}
	re.charset = "utf-8"
	re.req = req
	re.code = 200

	return re
}

// Returns cloned response w/o buffer
func (re *Response) metaClone() *Response {
	clone := newResponse(re.req)
	clone.code = re.code

	if re.headers != nil && len(re.headers) > 0 {
		h := clone.Header()
		for key, val := range re.headers {
			h[key] = val
		}
	}

	clone.contentType = re.contentType
	clone.charset = re.charset
	return clone
}

func (re *Response) Request() *Request {
	return re.req
}

func (re *Response) Code() int {
	return re.code
}

func (re *Response) SetCode(code int) {
	re.code = code
}

func (re *Response) IsError() bool {
	return re.code >= 500
}

func (re *Response) IsRedirect() bool {
	return re.code == http.StatusFound || re.code == http.StatusMovedPermanently
}

func (re *Response) HeaderEnt(name string) string {
	return re.headers.Get(name)
}

func (re *Response) SetContentType(ext string) (err error) {
	if strings.Contains(ext, "/") {
		re.contentType = ext
		return
	}

	if !strings.Contains(ext, ".") {
		ext = "." + ext
	}

	ct := mime.TypeByExtension(ext)
	if ct == "" {
		err = errors.New("%s: no MIME type found")
	} else if i := strings.Index(ct, "; charset="); i != -1 {
		re.contentType = ct[:i]
	} else {
		re.contentType = ct
	}
	return
}

// Header implements the header method of http.ResponseWriter
func (re *Response) Header() http.Header {
	re.once.Do(func() {
		re.headers = make(http.Header)
	})
	return re.headers
}

// WriteHeader implements the WriteHeader method of http.ResponseWriter
func (re *Response) WriteHeader(code int) {
	re.SetCode(code)
}

// Saves sessions
func (re *Response) saveSession() error {
	return re.req.Session.Save(re.req.Request, re)
}

// Apply takes an http.ResponseWriter and calls the required methods on it to
// output the buffered headers, response code, and data. It returns the number
// of bytes written and any errors flushing.
func (re *Response) Apply(w http.ResponseWriter) (n int, err error) {
	body := bytes.Trim(re.Bytes(), "\r\n\t ")
	if re.req.app.config["env"] == "production" { // some cleanup
		body = bytes.Replace(body, []byte("\n"), []byte(""), -1)
		body = bytes.Replace(body, []byte("  "), []byte(" "), -1)
		body = bytes.Replace(body, []byte("> "), []byte(">"), -1)
	}

	// plain text detection
	if re.contentType == "text/html" && bytes.Index(body, []byte("<")) == -1 {
		re.contentType = "text/plain"
	}

	re.Header().Set("Content-Type", fmt.Sprintf("%s; charset=%s", re.contentType, re.charset))
	re.Header().Set("Content-Length", strconv.Itoa(len(body)))

	if len(re.headers) > 0 {
		h := w.Header()
		for key, val := range re.headers {
			h[key] = val
		}
	}
	if re.code > 0 {
		w.WriteHeader(re.code)
	}
	n, err = w.Write(body)
	return
}

func (re *Response) dump() {
	fmt.Printf("Status: %d\n", re.code)
	re.Header().Set("Content-Length", fmt.Sprintf("%d", len(re.Bytes())))
	if len(re.headers) > 0 {
		for key, val := range re.headers {
			fmt.Printf("H %s: %s\n", key, val)
		}
	}

	fmt.Printf("\n[%s]\n", bytes.Trim(re.Bytes(), "\r\n\t "))
}

func (re *Response) Printf(format string, a ...interface{}) *Response {
	fmt.Fprintf(re, format, a...)
	return re
}

func (re *Response) Print(s string) *Response {
	fmt.Fprint(re, s)
	return re
}

func (re *Response) Redirect(urlStr string) *Response {
	u := path.Join(re.req.app.RoutePrefix, urlStr)
	http.Redirect(re, re.req.Request, u, http.StatusFound)
	return re
}

func (re *Response) PermanentRedirect(urlStr string) *Response {
	u := path.Join(re.req.app.RoutePrefix, urlStr)
	http.Redirect(re, re.req.Request, u, http.StatusMovedPermanently)
	return re
}

func (re *Response) NotModified() *Response {
	http.Error(re, "Not Modified", http.StatusNotModified)
	return re
}

func statusText(status int, m ...interface{}) string {
	if len(m) == 0 && status > 0 {
		return http.StatusText(status)
	}
	format, ok := m[0].(string)
	if ok && strings.Index(format, "%") >= 0 {
		return fmt.Sprintf(format, m[1:]...)
	}

	return fmt.Sprint(m...)
}

func (re *Response) Error(status int, a ...interface{}) *Response {
	if status == 0 {
		status = http.StatusInternalServerError
	}
	message := statusText(status, a...)
	http.Error(re, message, status)
	return re
}

func (re *Response) BadRequest(a ...interface{}) *Response {
	return re.Error(http.StatusBadRequest, a...)
}

func (re *Response) NotFound(a ...interface{}) *Response {
	return re.Error(http.StatusNotFound, a...)
}

func (re *Response) NotAcceptable(a ...interface{}) *Response {
	return re.Error(http.StatusNotAcceptable, a...)
}

func (re *Response) Forbidden(a ...interface{}) *Response {
	return re.Error(http.StatusForbidden, a...)
}

func (re *Response) Unauthorized(a ...interface{}) *Response {
	return re.Error(http.StatusUnauthorized, a...)
}

func (re *Response) Error500(a ...interface{}) *Response {
	return re.Error(http.StatusInternalServerError, a...)
}
