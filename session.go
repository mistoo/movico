package movico

import (
	"fmt"
	"os"
	"path"

	"github.com/gorilla/sessions"
)

type SessionStore interface {
	Name() string
	NewStore(config map[string]string) (sessions.Store, error)
}

var sessionStores = newRegistry("sessionStores")

func RegisterSessionStore(store SessionStore) {
	sessionStores.set(store)
}

func getSessionStore(name string) (SessionStore, error) {
	store, ok := sessionStores.get(name)
	if !ok {
		return nil, fmt.Errorf("%s: no such session store registered", name)
	}
	return store.(SessionStore), nil
}

type CookieStore sessions.CookieStore

func (s *CookieStore) Name() string {
	return "cookie"
}

func (s *CookieStore) NewStore(config map[string]string) (sessions.Store, error) {
	var store sessions.Store = sessions.NewCookieStore([]byte(config["session.secret"]))
	store.(*sessions.CookieStore).Options = &sessions.Options{Path: "/", MaxAge: 0}
	return store, nil
}

type FilesystemStore sessions.CookieStore

func (s *FilesystemStore) Name() string {
	return "file"
}

func (s *FilesystemStore) NewStore(config map[string]string) (sessions.Store, error) {
	p := path.Join(config["root"], "/tmp/sessions")
	file, err := os.OpenFile(p+"/testing", os.O_RDWR|os.O_CREATE, 0644)
	if err == nil {
		file.Close()
	} else {
		return nil, fmt.Errorf("%s: invalid sessions directory (%s)", p, err)
	}

	var store sessions.Store = sessions.NewFilesystemStore(p, []byte(config["session.secret"]))
	store.(*sessions.FilesystemStore).Options = &sessions.Options{Path: "/", MaxAge: 0}
	return store, nil
}

func init() {
	RegisterSessionStore(new(CookieStore))
	RegisterSessionStore(new(FilesystemStore))
}
