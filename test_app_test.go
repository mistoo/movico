package movico

import (
	"fmt"
)

// models
type Person struct {
	Id string
}

func (p *Person) ObjectId() string {
	return p.Id
}

type Mouse struct {
	Id string
}

func (p *Mouse) ObjectId() string {
	return p.Id
}

type Alias struct {
	Id   int    `json:"id" xml:"id,attr"`
	Name string `json:"name" xml:"name"`
	To   string `json:"to" xml:"to"`
}

// Controllers
// Application parent controller
type ApplicationController struct {
	Controller
	initCalled   int
	beforeCalled int
	afterCalled  int
}

func (c *ApplicationController) Init() {
	c.initCalled += 1
}

func (c *ApplicationController) IsLoggedIn(req *Request) bool {
	_, ok := req.Session.Values["userId"]
	return ok
}

func (c *ApplicationController) Before(req *Request) *Response {
	c.beforeCalled += 1
	if c.IsLoggedIn(req) {
		return nil
	}
	switch {
	case req.MethodFQN == "People.Edit":
		req.RedirectResponse("/login")
	default:
		return nil
	}
	req.Session.Values["back_to"] = req.URL.RequestURI()
	return req.RedirectResponse("/login")
}

func (c *ApplicationController) After(re *Response) {
	c.afterCalled += 1
}

// REST
type People struct {
	ApplicationController
}

func (c *People) Init() {

}

func (c *People) Index(r *Request) *Response {
	return r.Response("People Index")
}

func (c *People) Show(r *Request, id string) *Response {
	return r.Response("Person id=%s", id)
}

func (c *People) Edit(r *Request, id string) *Response {
	return r.Response("Edit Person id=%s", id)
}

// Custom routes
type Mice struct {
	ApplicationController
	initCalled   int
	beforeCalled int
	afterCalled  int
}

func (c *Mice) Init() {
	c.ApplicationController.Init() // super()
	c.initCalled += 1

	c.SetDefaultFormats("html", "json", "xml")
	c.AddRoute("Foo", "/foo")
	//c.AddRoute("Foo", "/fooj").Formats("json")
	//c.AddRoute("Foo", "/foox").Formats("xml")
	c.AddRoute("Bar", "/bar/{id}").Formats("html", "json")
	c.AddRoute("Baz", "/baz/{year}/{month}")
	c.AddRoute("Panic", "/panic")
}

func (c *Mice) Before(req *Request) *Response {
	if re := c.ApplicationController.Before(req); re != nil {
		return re
	}
	c.beforeCalled += 1
	return nil
}

func (c *Mice) After(re *Response) {
	c.ApplicationController.After(re)
	c.afterCalled += 1
}

func (c *Mice) Show(r *Request, id int) *Response {
	r.Session.Values["testValue"] = 2 * id
	r.Session.Values["id"] = id
	return r.Response("Mouse ", "id=", id)
}

func (c *Mice) Foo(r *Request) *Response {
	return r.Response("Foo")
}

func (c *Mice) Bar(r *Request, id int) *Response {
	return r.Response("Bar ", "id=", id)
}

func (c *Mice) Baz(r *Request, year int, month int) *Response {
	return c.Render(r, "year", year, "month", month) // no template exists => should panic with 500
}

func (c *Mice) Panic(r *Request) *Response {
	panic("aaa")
}

// Render without layout
type Aliases struct {
	ApplicationController
}

func (c *Aliases) Init() {
	c.SetLayout("")
	c.SetDefaultFormats("html", "json", "xml")
}

func (c *Aliases) Show(r *Request, id int) *Response {
	a := &Alias{id, fmt.Sprintf("name_%d", id), fmt.Sprintf("to_%d", id)}
	return c.Render(r, "alias", a)
}

// CamelCase controller, to test reflect
type MailAliases struct {
	ApplicationController
}

func (c *MailAliases) Show(r *Request, id int) *Response {
	return c.Render(r, "mailAlias", &Alias{id, "foo", "bar"})
}

// With Controller suffix and non-standard /foo/bar/fish route path
type FishController struct {
	ApplicationController
}

func (c *FishController) Init() {
	c.SetRoutePath("/foo/bar/fish")
}

func (c *FishController) Show(r *Request, id int) *Response {
	return r.Response("Fish id=%d", id)
}
