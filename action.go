package movico

import (
	"errors"
	"fmt"
	"reflect"
	"regexp"
	"strconv"
)

type actionMethod struct {
	method         reflect.Value
	pathVars       []string
	methodArgTypes []reflect.Kind
	beforeFilter   reflect.Value
	afterFilter    reflect.Value
}

func newActionMethod(methodName string, ci ControllerInterface, routePath string) *actionMethod {
	methodFQN := ci.Name() + "." + methodName
	method := reflect.ValueOf(ci).MethodByName(methodName)

	if (method == reflect.Value{}) {
		panic(fmt.Sprintf("%s: method not found", methodFQN))
	}

	pathVars := extractPathVars(routePath)
	methodArgTypes, err := verifyMethod(method, pathVars)

	if err != nil {
		panic(fmt.Sprintf("%s: %s", methodFQN, err))
	}

	beforeFilter := reflect.ValueOf(ci).MethodByName("Before")
	afterFilter := reflect.ValueOf(ci).MethodByName("After")

	return &actionMethod{method, pathVars, methodArgTypes, beforeFilter, afterFilter}
}

func extractPathVars(path string) []string {
	re := regexp.MustCompile("/{([^}]+)}")

	matches := re.FindAllStringSubmatch(path, -1)
	vars := make([]string, 0, len(matches))
	for _, m := range matches {
		vars = append(vars, m[1])
	}
	return vars
}

/*
 Route path variables are passed as arguments
  foos/                -> ControllerFoo.Index(* Request)  // no params
  foos/{page}          -> ControllerFoo.Index(* Request, page int)
  foos/{id}/edit/{bar} -> ControllerFoo.Index(* Request, id int, bar string)
*/
func verifyMethod(method reflect.Value, pathVars []string) ([]reflect.Kind, error) {
	t := method.Type()

	// Verify method function outcomes
	nouts := t.NumOut()
	outErr := errors.New("method must return one parameter: string or *movico.Response")
	if nouts != 1 {
		return nil, outErr
	}

	out := t.Out(0) // first must be a string or *Response
	if out != reflect.TypeOf((*string)(nil)).Elem() && out != reflect.TypeOf((*Response)(nil)) {
		return nil, outErr
	}

	// Verify method function arguments
	nargs := t.NumIn()

	ferr := errors.New("method first argument must be *movico.Request")
	if nargs == 0 {
		return nil, errors.New("method must have at least one *movico.Request argument")
	}

	in := t.In(0)
	if in.Kind() != reflect.Ptr {
		return nil, ferr
	}

	if reflect.Type(in) != reflect.TypeOf((*Request)(nil)) {
		return nil, ferr
	}

	if len(pathVars) != nargs-1 {
		return nil, fmt.Errorf("method arguments must be the same as specified in route path: %v", pathVars)
	}

	argtypes := make([]reflect.Kind, 0, nargs-1)

	for i := 1; i < nargs; i++ {
		in = t.In(i)
		argtypes = append(argtypes, in.Kind())
	}

	return argtypes, nil
}

func (am *actionMethod) doCallMethod(req *Request) ([]reflect.Value, error) {
	in := make([]reflect.Value, len(am.methodArgTypes)+1)
	in[0] = reflect.ValueOf(req)
	n := 1

	for _, name := range am.pathVars {
		v := req.muxVars[name]
		kind := am.methodArgTypes[n-1]
		varg := reflect.Value{}

		switch kind {
		case reflect.Ptr:
			varg = reflect.ValueOf(nil)

		case reflect.Int, reflect.Uint:
			vi, _ := strconv.Atoi(v)
			varg = reflect.ValueOf(vi)

		case reflect.String:
			varg = reflect.ValueOf(v)

		default:
			return nil, fmt.Errorf("%s: invalid type %s", v, kind)
		}

		if (varg != reflect.Value{}) {
			in[n] = varg
		}
		n += 1
	}

	rv := am.method.Call(in)
	if len(rv) != 1 {
		return nil, fmt.Errorf("%s(): Wrong number (%d) of parameters returned, need %d", req.MethodFQN, len(rv), 1)
	}

	return rv, nil
}

func (am *actionMethod) call(req *Request) (re *Response) {
	if (am.beforeFilter != reflect.Value{}) {
		re = am.callBeforeFilter(req)
	}

	if re == nil { // skip method call if Before() returns Response
		re = am.callMethod(req)
	}

	if (re != nil && !re.IsError() && am.afterFilter != reflect.Value{}) {
		am.callAfterFilter(re)
	}

	return re
}

func (am *actionMethod) callMethod(req *Request) (re *Response) {
	rv, err := am.doCallMethod(req)
	if err != nil {
		panic(err)
	}

	if content, ok := rv[0].Interface().(string); ok {
		re = req.Response(content)

	} else if response, ok := rv[0].Interface().(*Response); ok {
		if response == nil {
			panic(fmt.Errorf("%s: returned nil *Response pointer", req.MethodFQN))
		}
		re = response
	}
	return re
}

func (am *actionMethod) callBeforeFilter(req *Request) *Response {
	in := make([]reflect.Value, 1)
	in[0] = reflect.ValueOf(req)
	rv := am.beforeFilter.Call(in)

	if len(rv) == 0 {
		return nil
	}
	iface := rv[0].Interface()
	if re, ok := iface.(*Response); ok {
		return re
	} else {
		panic(fmt.Errorf("Before() filter %s returns non-movico.Response", req.MethodFQN))
	}
	return nil
}

func (am *actionMethod) callAfterFilter(re *Response) {
	if (am.afterFilter == reflect.Value{}) {
		return
	}

	in := make([]reflect.Value, 1)
	in[0] = reflect.ValueOf(re)
	am.afterFilter.Call(in)
}
