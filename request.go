package movico

import (
	"fmt"
	"mime/multipart"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
)

type Request struct {
	*http.Request
	ControllerName string
	MethodName     string
	MethodFQN      string
	TemplatePath   string
	Session        *sessions.Session
	DbSession      DatabaseSession
	app            *Application
	formParsed     bool
	muxVars        map[string]string
	Format         string
	createdAt      time.Time
}

func NewRequest(r *http.Request, controllerName, methodName, templatePath string) *Request {
	fqn := controllerName + "." + methodName
	req := Request{Request: r, ControllerName: controllerName, MethodName: methodName, MethodFQN: fqn, TemplatePath: templatePath}
	req.createdAt = time.Now()

	//if r.Path

	req.muxVars = mux.Vars(r)
	req.Format = req.getFormat()
	if req.muxVars == nil {
		req.muxVars = map[string]string{}
	}
	req.muxVars["format"] = req.Format
	return &req
}

func (r *Request) Duration() time.Duration {
	return time.Now().Sub(r.createdAt)
}

func (r *Request) getFormat() string {
	uri := r.Request.URL.Path
	i := strings.LastIndex(uri, ".")
	if i == -1 {
		return "html"
	}

	format := uri[i:]
	if len(format) > 10 { // TODO validaton: non-ascii, etc
		return "html"
	}

	if r.muxVars == nil {
		return format[1:]
	}

	for k, v := range r.muxVars {
		if fi := strings.LastIndex(v, format); fi != -1 && fi+len(format) == len(v) {
			v = v[:fi]
			r.muxVars[k] = v
		}
	}

	return format[1:]
}

func (r *Request) Response(a ...interface{}) *Response {
	re := newResponse(r)
	if len(a) > 0 {
		re.Print(statusText(0, a...))
	}
	return re
}

func (r *Request) Scheme() string {
	s := r.Request.Header.Get("X-Forwarded-Proto")
	if s == "" {
		s = "http"
	}
	return s
}

func (r *Request) ErrorResponse(err error) *Response {
	return r.Response().Error500("%v", err)
}

func (r *Request) RedirectResponse(urlStr string) *Response {
	return newResponse(r).Redirect(urlStr)
}

func (r *Request) PermanentRedirectResponse(urlStr string) *Response {
	return newResponse(r).PermanentRedirect(urlStr)
}

func (r *Request) NotModifiedResponse() *Response {
	return newResponse(r).NotModified()
}

func (r *Request) BadRequestResponse(a ...interface{}) *Response {
	return newResponse(r).BadRequest(a...)
}

func (r *Request) NotFoundResponse(a ...interface{}) *Response {
	return newResponse(r).NotFound(a...)
}

func (r *Request) ForbiddenResponse(a ...interface{}) *Response {
	return newResponse(r).Forbidden(a...)
}

func (r *Request) UnauthorizedResponse(a ...interface{}) *Response {
	return newResponse(r).Unauthorized(a...)
}

func (r *Request) AbsolutizeURL(u *url.URL) *url.URL {
	u.Host = r.Host
	u.Scheme = r.Scheme()
	return u
}

func (r *Request) ParseForm() {
	if r.formParsed {
		return
	}

	r.Request.ParseMultipartForm(1024 * 16)
	for k, v := range r.muxVars {
		r.Form[k] = []string{v}
	}

	if r.Request.MultipartForm != nil && r.Request.MultipartForm.File != nil {
		for key, fhs := range r.MultipartForm.File {
			if len(fhs) > 0 {
				r.Form[key] = []string{"FILE"}
			}
		}
	}

	r.formParsed = true
}

func (r *Request) Params() map[string][]string {
	r.ParseForm()
	return r.Form
}

type File struct {
	multipart.File
	Header *multipart.FileHeader
}

func (r *Request) File(name string) (*File, error) {
	r.ParseForm()
	/*if v, ok := r.Form[name]; !ok || v[0] != "FILE" {
		return nil, nil, fmt.Errorf("%s: no such http file (%+v)", name, v)
	}*/
	fmt.Printf("MM %+v\n", r.MultipartForm.File)
	if fhs := r.MultipartForm.File[name]; len(fhs) > 0 {
		f, err := fhs[0].Open()
		return &File{f, fhs[0]}, err
	}
	return nil, fmt.Errorf("%s: no such http file")
}

// model[field1]=1&model[field2]=2 => { field1: 1, field2: 2 }
func (r *Request) ModelFile(model Modeler, name string) (file *File, err error) {
	modelName, err := ModelerName(model)
	if err != nil {
		panic(err)
	}

	file, err = r.File(fmt.Sprintf("%s[%s]", modelName, name))
	if err != nil {
		file, err = r.File(name)
	}
	return
}

// model[field1]=1&model[field2]=2 => { field1: 1, field2: 2 }
func (r *Request) ModelParams(model Modeler) map[string][]string {
	name, err := ModelerName(model)
	if err != nil {
		panic(err)
	}

	params := r.Params()

	values := map[string][]string{}
	for k, v := range params {
		if strings.HasPrefix(k, name) {
			i0 := strings.Index(k, "[")
			i1 := strings.Index(k, "]")
			if i0 != -1 && i1 != -1 {
				if len(v) > 0 && v[0] == "FILE" { // omit posted files
					continue
				}
				values[k[i0+1:i1]] = v
				fmt.Printf("ModelParam %s -> %s.%s\n", k, k[0:i0], k[i0+1:i1])
			}
		}
	}

	if len(values) == 0 { // no model[field] params?
		return params
	}
	return values
}
