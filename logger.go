package movico

// Logger, based on https://github.com/cratonica/clog by Clint Caywood

import (
	"fmt"
	"html/template"
	"io"
	"os"
	"runtime"
	"strings"
	"sync"
	"time"
)

type logLevel uint8

const (
	logLevelFatal logLevel = iota
	logLevelError
	logLevelWarning
	logLevelInfo
	logLevelDebug
	logLevelTrace

	logAccessLog = 99
)

var logLevelLabels = map[logLevel]string{
	logLevelFatal:   "fatal",
	logLevelError:   "error",
	logLevelWarning: "warning",
	logLevelInfo:    "info",
	logLevelDebug:   "debug",
	logLevelTrace:   "trace",
	logAccessLog:    "access",
}

func (l logLevel) String() string {
	label := logLevelLabels[l]
	if len(label) == 0 {
		return fmt.Sprintf("Unknown LogLevel: %d", l)
	}
	return label
}

type output struct {
	writer io.Writer
	level  logLevel
}

type Logger struct {
	mutex   sync.Mutex
	outputs []output
}

func newLogger() *Logger {
	return &Logger{sync.Mutex{}, make([]output, 0)}
}

// Adds an ouput, specifying the maximum log LogLevel
// you want to be written to l output. For instance,
// if you pass Warning for logLevel, all logs of type
// Warning, Error, and Fatal would be logged to l output.
func (l *Logger) AddOutput(writer io.Writer, logLevelLabel string) {
	l.mutex.Lock()
	defer l.mutex.Unlock()

	var level logLevel = 0
	for k, v := range logLevelLabels {
		if logLevelLabel == v {
			level = k
			break
		}
	}

	l.outputs = append(l.outputs, output{writer, level})
}

func (l *Logger) Trace(format string, v ...interface{}) {
	l.Log(logLevelTrace, format, v...)
}

func (l *Logger) Debug(format string, v ...interface{}) {
	l.Log(logLevelDebug, format, v...)
}

func (l *Logger) Info(format string, v ...interface{}) {
	l.Log(logLevelInfo, format, v...)
}

func (l *Logger) Warning(format string, v ...interface{}) {
	l.Log(logLevelWarning, format, v...)
}

func (l *Logger) Error(format string, v ...interface{}) {
	l.Log(logLevelError, format, v...)
}

func (l *Logger) Fatal(format string, v ...interface{}) {
	l.Log(logLevelFatal, format, v...)
}

// Apache style logging 
func (l *Logger) LogAccess(re *Response) {
	req := re.req
	remoteaddr := strings.Split(req.RemoteAddr, ":")
	l.Log(logAccessLog, `%s "%s %s %s" %d %d %v`, remoteaddr[0], req.Method, req.RequestURI, req.Proto, re.Code(), re.Len(), req.Duration())
}

func timestamp() string {
	t := time.Now()
	return fmt.Sprintf("%v-%02d-%02d %02d:%02d:%02d.%03d", t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second(), t.Nanosecond()/1000000)
}

func (l *Logger) Log(level logLevel, format string, v ...interface{}) {
	message := fmt.Sprintf(format, v...) + "\n"
	ts := timestamp()
	prefix := fmt.Sprintf("%s [%-7s]", ts, level.String())

	prefixB := []byte(prefix)
	messageB := []byte("~ " + message)

	l.mutex.Lock()
	defer l.mutex.Unlock()

	for _, output := range l.outputs {
		if output.level >= level {
			file, ok := output.writer.(*os.File)

			if ok && file == os.Stdout { // no prefixed logs on Stdout
				output.writer.Write(messageB)
			} else {
				output.writer.Write(prefixB)
				output.writer.Write(messageB)
			}
		}
	}
}

type backtraceEntry struct {
	function string
	file     string
	line     int
	class    string
}

func (app *Application) backtrace() []*backtraceEntry {

	pc := make([]uintptr, 20)
	n := runtime.Callers(2, pc)

	trace := []*backtraceEntry{}
	start := false
	for i := 1; i < n; i++ {
		if f := runtime.FuncForPC(pc[i]); f != nil {
			file, line := f.FileLine(pc[i])
			name := f.Name()
			class := "runtime"
			if strings.HasPrefix(name, "mistoo/movico.") {
				class = "framework"
			} else if strings.HasPrefix(name, "_") {
				class = "application"
				start = true
			}

			if start == false {
				continue
			}

			file = strings.Replace(file, runtime.GOROOT(), "", -1)
			file = strings.Replace(file, app.RootPath+"/", "", -1)
			name = strings.Replace(name, app.RootPath+"/", "", -1)
			trace = append(trace, &backtraceEntry{name, file, line, class})
		}
	}
	return trace
}

func (app *Application) Backtrace(format string) template.HTML {
	trace := app.backtrace()
	var r template.HTML
	
	if format == "html" {
		l := ""
		for i := 0; i < len(trace); i++ {
			e := trace[i]
			l += fmt.Sprintf(`<dt class="%s">%s()</dt>`, e.class, e.function)
			l += fmt.Sprintf(`<dd class="%s">%s:%d</dd>`, e.class, e.file, e.line)
		}
		r = template.HTML(`<dl class="backtrace">` + l + `</dl>`)
	}
	return r
}
