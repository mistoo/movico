package mongo

import (
	"labix.org/v2/mgo"
	"mistoo/movico"
)

type MongoDatabase struct {
	Session  *mgo.Session
	Database *mgo.Database
	Url      string
	DbName   string
	//Models   []Modeler
}

func (e *MongoDatabase) Name() string {
	return "mongo"
}

type MongoSession struct {
	*mgo.Session
}

func (s *MongoSession) Close() {
	s.Session.Close()
	s.Session = nil
}

func (db *MongoDatabase) Connect(config map[string]string) error {
	session, err := mgo.Dial(config["url"])
	if err != nil {
		return err
	}
	session.SetMode(mgo.Monotonic, true)

	db.Session = session
	db.Database = session.DB(config["database"])
	db.Url = config["url"]
	db.DbName = config["database"]
	
	//for _, m := range Models {
	//	RegisterIndexes(m)
	//}

	//fmt.Printf("Connected to mongodb on %s, using \"%s\"\n", url, database)
	return nil
}

func (db *MongoDatabase) OpenSession() movico.DatabaseSession {
	session := db.Session.Clone()
	return &MongoSession{session}
}

func (db *MongoDatabase) Disconnect() {
	db.Session.Close()
	db.Session = nil
	db.Database = nil
}

func init() {
	movico.RegisterDatabase(new(MongoDatabase))
}

