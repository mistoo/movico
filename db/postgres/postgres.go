package postgres

import (
	"labix.org/v2/mgo"
	"mistoo/movico"
)

type PostgresEngine int

func (e *PostgresEngine) Name() string {
	return "postgres"
}

func (e *PostgresEngine) NewEngine() movico.Database {
	return &PostgresDatabase{}
}

type PostgresDatabase struct {
	Session  *mgo.Session
	Database *mgo.Database
	Url      string
	DbName   string
	//Models   []Modeler
}


func (db *PostgresDatabase) Connect(config map[string]string) error {
	session, err := mgo.Dial(config["url"])
	if err != nil {
		return err
	}
	session.SetMode(mgo.Monotonic, true)

	db.Session = session
	db.Database = session.DB(config["database"])
	db.Url = config["url"]
	db.DbName = config["database"]
	
	//for _, m := range Models {
	//	RegisterIndexes(m)
	//}

	//fmt.Printf("Connected to mongodb on %s, using \"%s\"\n", url, database)
	return nil
}

func (db *PostgresDatabase) Open(s interface{}) {
	s.(*mgo.Session).Clone()
}

func (db *PostgresDatabase) Close(s interface{}) {
	s.(*mgo.Session).Close()
}


func (db *PostgresDatabase) Disconnect() {
	db.Session.Close()
	db.Session = nil
	db.Database = nil
}

func init() {
	movico.RegisterDatabaseEngine(new(PostgresEngine))
}

