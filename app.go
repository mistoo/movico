package movico

import (
	"errors"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"reflect"
	"strings"

	"bitbucket.org/pkg/inflect"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
)

type Application struct {
	router       *mux.Router
	RoutePrefix  string // route prefix (path prefix)
	StaticPrefix string // /static

	// application paths
	RootPath   string // app root directory
	StaticPath string // static files directory
	ViewsPath  string // template root path
	LogPath    string // log directory

	sessionStore sessions.Store
	TemplateEngine   TemplateEngine
	db           Database

	SchemaDecoder *Decoder

	defaultConfig map[string]string
	argpConfig    map[string]string
	config        map[string]string
	dbConfig      map[string]string // database config

	controllers    map[string]ControllerInterface
	routes         map[string]*Route

	Log       *Logger
	accessLog *Logger
	listener  net.Listener
	pidfile   *os.File
}

func (app *Application) newRequest(r *http.Request, controllerName, methodName, templatePath string) *Request {
	session, _ := app.sessionStore.Get(r, app.config["sessions.id"])

	req := NewRequest(r, controllerName, methodName, templatePath)
	req.Session = session
	if app.db != nil {
		req.DbSession = app.db.OpenSession()
	}
	req.app = app
	return req
}

func (app *Application) deleteRequest(req *Request) {
	if req.DbSession != nil {
		req.DbSession.Close()
	}
	req.app = nil
	req.Session = nil
	req.DbSession = nil
}

func NewApplication() *Application {
	return newApplication("app.conf", true)
}

func newApplication(configPath string, verbose bool) *Application {
	defer func() {
		if r := recover(); r != nil {
			fmt.Printf("%s\n", r)
			os.Exit(1)
		}
	}()

	app := Application{}
	app.router = mux.NewRouter().StrictSlash(true)
	//app.router.NotFoundHandler = &app

	app.controllers = make(map[string]ControllerInterface)
	app.routes = make(map[string]*Route)
	app.SchemaDecoder = NewDecoder()

	app.setDefaultConfig()
	app.parseArgp()

	if verbose {
		fmt.Printf("Movico root at %s\n", app.rootPath())
	}

	if configPath != "" {
		app.loadConfig(configPath, verbose)
	}

	// logger
	app.Log = newLogger()
	if app.config["env"] != "production" {
		app.Log.AddOutput(os.Stdout, "trace")
	} else {
		if verbose {
			fmt.Printf("Logging to %s\n", app.LogPath)
		}
		file, err := os.OpenFile(app.LogPath, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0640)
		if err != nil {
			panic(fmt.Errorf("Cannot open %s: %s", app.LogPath, err))
		}
		app.Log.AddOutput(file, "info")
	}

	// access.log logger
	accessLogPath := path.Join(app.RootPath, "log", "access.log")
	file, err := os.OpenFile(accessLogPath, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0640)
	if err != nil {
		panic(fmt.Errorf("Cannot open %s: %s", accessLogPath, err))
	}
	app.accessLog = newLogger()
	app.accessLog.AddOutput(file, "access")

	if verbose {
		app.Log.Info("%s root at %s", app.config["name"], app.rootPath())
	}

	if app.config["templates"] != "" {
		engine, err := getTemplateEngine(app.config["templates"])
		if err != nil {
			panic(err)
		}
		app.TemplateEngine = engine
	}

	if app.config["sessions.store"] != "" {
		s, err := getSessionStore(app.config["sessions.store"])
		if err != nil {
			panic(err)
		}
		app.sessionStore, err = s.NewStore(app.config)
		if err != nil {
			panic(err)
		}
	}

	if app.dbConfig["adapter"] != "" {
		db, err := getDatabase(app.dbConfig["adapter"])
		if err != nil {
			panic(err)
		}

		err = db.Connect(app.dbConfig)
		if err != nil {
			panic(err)
		}
		app.db = db
		app.Log.Info("Connected to database %#v", app.dbConfig)
	}

	return &app
}

func (app *Application) rootPath() string {
	if root, ok := app.defaultConfig["root"]; ok && root != "" {
		app.RootPath = path.Clean(root)
	}

	if app.RootPath == "" {
		p := path.Clean(os.Args[0])
		if strings.HasPrefix(p, "/") {
			app.RootPath = filepath.Dir(p)
		} else {
			wd, err := os.Getwd()
			if err != nil {
				panic(fmt.Errorf("Cannot determine app root: Getwd() failed: %v", err))
			}
			app.RootPath = filepath.Dir(path.Join(wd, p))
		}
	}
	return app.RootPath
}

func (app *Application) relativePath(path string) string {
	return strings.Replace(path, app.RootPath, ".", 1)
}

// passing to mux
func (app *Application) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	app.Log.Info("Request %s %s", r.Method, r.URL.Path)
	app.router.ServeHTTP(w, r)
}

func (app *Application) defaultErrorHandler(w http.ResponseWriter, req *Request, err error) {
	r := recover()
	if r == nil {
		return
	}

	app.Log.Error("Double panic: %+v", r)
	app.Log.Error("Panic: %+v", err)

	re := req.Response().Error500(err)
	re.Printf("<br />")
	re.Printf("%s", app.Backtrace("html"))
	re.Apply(w)
}

func (app *Application) renderedErrorHandler(w http.ResponseWriter, req *Request) {
	r := recover()
	if r == nil {
		return
	} 
	
	if _, ok := r.(error); !ok {
		r = fmt.Errorf("%+v", r)
	}

	app.Log.Error("Panic: %+v", r)
	re := req.ErrorResponse(r.(error))
	
	if renderer, err := app.newRenderer(req); err == nil {
		rre := re.metaClone()	// clone response w/o body

		if err := renderer.RenderError(rre, re); err != nil {
			app.Log.Error("RenderError(): %v %v", err, re.String())
		} else if rre.Len() == 0 { // nothing rendered
			app.Log.Error("RenderError(): empty response, %v", re.Code())
		} else {
			re = rre
		}

	} else {
		re.Printf("e %v<br/>", err)
		re.Printf("%v<br/>", r)
		re.Printf("%s", app.Backtrace("html"))
	}
	re.Apply(w)
}

func (app *Application) makeHandlerFunc(methodName string, ci ControllerInterface, routePath string) http.HandlerFunc {
	action := newActionMethod(methodName, ci, routePath)
	methodFQN := ci.Name() + "." + methodName
	templatePath := ci.controllerInstance().methodTemplatePath(methodName)

	h := func(w http.ResponseWriter, r *http.Request) {
		req := app.newRequest(r, ci.Name(), methodName, templatePath)
		defer app.deleteRequest(req)
		defer app.renderedErrorHandler(w, req)

		route := app.routes[methodFQN]
		
		app.Log.Info("  routed to %s(), request format %s", req.MethodFQN, req.Format)
		app.Log.Info("  params %+v", req.Params())
		if req.Session != nil {
			app.Log.Info("  session %+v", req.Session.Values)
		}

		var re *Response
		
		if route.respondsTo(req.Format) {
			re = action.call(req)

		} else {
			re = req.Response().NotAcceptable("%s is not available for this resource", req.Format)
		}

		if err := re.saveSession(); err != nil {
			panic(err)
		}

		if re.Code() > 299 {
			renderer, err := app.newRenderer(re.req)
			if err != nil {
				rre := re.metaClone() // RenderError to cloned response
				if err := renderer.RenderError(rre, re); err != nil && rre.Len() > 0 {
					re = rre
				}
			}
		}

		re.Apply(w)

		go func() {
			app.Log.Info("%p %s in %v", req, req.MethodFQN, req.Duration())
			app.accessLog.LogAccess(re)
			
			if re.IsRedirect() {
				app.Log.Info("Redirect to %s", re.HeaderEnt("Location"))
			}
		}()
	}

	return h
}

func (app *Application) initController(ci ControllerInterface) {
	vci := reflect.ValueOf(ci)

	controllerName := reflect.Indirect(vci).Type().Name()
	ci.controllerInstance().initializeController(controllerName, app)

	initm := vci.MethodByName("Init") // call Init() if any
	if (initm != reflect.Value{}) {
		in := make([]reflect.Value, 0)
		initm.Call(in)
	}

	app.controllers[ci.Name()] = ci
}

func (app *Application) AddController(ci ControllerInterface) {
	app.initController(ci)

	app.setupController(ci)
}

func (app *Application) findRoute(toController, toMethod string) (*Route, error) {
	ci := app.controllers[toController]
	if ci == nil {
		panic(fmt.Errorf("%s: no such controller found"))
	}
	methodFQN := fmt.Sprintf("%s.%s", ci.Name(), toMethod)
	if route, ok := app.routes[methodFQN]; ok {
		return route, nil
	}
	return nil, fmt.Errorf("%s: route not found", methodFQN)
}

func (app *Application) URL(toController, toMethod string, pairs ...string) (*url.URL, error) {
	if len(pairs)%2 != 0 {
		panic(fmt.Errorf("Odd number of arguments (%d)", len(pairs)))
	}

	route, err := app.findRoute(toController, toMethod)
	if err != nil {
		return nil, err
	}

	pathArgs := []string{}
	query := url.Values{}

	for i := 0; i < len(pairs); i = i + 2 {
		if _, ok := route.pathArgs[pairs[i]]; ok {
			pathArgs = append(pathArgs, pairs[i], pairs[i+1])
		} else {
			query.Add(pairs[i], pairs[i+1])
		}
	}

	u, err := route.URL(pathArgs...)
	if err != nil {
		return nil, fmt.Errorf("%s.%s: mux.Route.URL() error: %v", toController, toMethod, err)
	}
	u.RawQuery = query.Encode()

	return u, nil
}

func (app *Application) Url(toController, toMethod string, pairs ...string) string {
	u, err := app.URL(toController, toMethod, pairs...)
	if err != nil {
		panic(err)
	}
	return u.String()
}

func (app *Application) ModelUrl(model Modeler) string {
	v := reflect.ValueOf(model)
	if v.Kind() != reflect.Ptr || v.Elem().Kind() != reflect.Struct {
		panic(errors.New("ModelUrl: Modeler must be a pointer to struct"))
	}

	controllerName := inflect.Capitalize(inflect.Pluralize(strings.ToLower(reflect.Indirect(v).Type().Name())))
	return app.Url(controllerName, "Show", "id", model.ObjectId())
}

func (app *Application) SetDefaultRoute(toController, toMethod string) error {
	route, err := app.findRoute(toController, toMethod)
	if err != nil {
		return err
	}

	defaultRoute := app.router.Path("/").Handler(route.GetHandler()).Methods("GET")
	app.Log.Trace("GET / routed to %s.%s", toController, toMethod)

	return defaultRoute.GetError()
}

func (app *Application) registerRoute(methodFQN string, route *Route) {
	app.routes[methodFQN] = route
}

func (app *Application) setupController(ci ControllerInterface) {
	app.Log.Debug("%s routes (%s)", ci.Name(), ci.RoutePath())

	t := reflect.TypeOf(ci)
	for i := 0; i < t.NumMethod(); i++ {
		method := t.Method(i)

		cr := ci.RouteFor(method.Name)
		if cr == nil {
			continue
		}

		methodFQN := fmt.Sprintf("%s.%s", ci.Name(), method.Name)

		if cr.path == "" {
			panic(fmt.Errorf("Empty route path for %s", methodFQN))
		}

		if methodFQN != cr.methodFQN {
			panic(fmt.Errorf("%s: route signature mismatch %s != %s", cr.path, cr.methodFQN, methodFQN))
		}

		h := app.makeHandlerFunc(method.Name, ci, cr.path)
		cr.handlerFunc(h)

		rpath := path.Join(ci.RoutePath(), cr.path)

		route := app.router.Path(rpath)
		if _, err := cr.apply(route); err != nil {
			panic(err)
		}

		// add .<format> routes
		for _, format := range cr.formats {
			//fmt.Printf("DEBUG formats %s\n", format)
			route := app.router.Path(rpath + "." + format)
			if _, err := cr.apply(route); err != nil {
				panic(err)
			}
		}

		if cr.methods != nil && (cr.methods[0] == "PUT" || cr.methods[0] == "DELETE") {
			routePOST := app.router.Path(rpath)
			crPOST := *cr
			crPOST.Queries("_method", strings.ToLower(cr.methods[0]))
			crPOST.Method("POST")
			if _, err := crPOST.apply(routePOST); err != nil {
				panic(err)
			}
			app.Log.Debug("    %s %s routed to %s", crPOST.methods[0], path.Join(ci.RoutePath(), cr.path), methodFQN)
		}

		cr.muxRoute = route
		app.registerRoute(methodFQN, cr)
		app.Log.Debug("  %s %s routed to %s", cr.methods[0], path.Join(ci.RoutePath(), cr.path), methodFQN)
	}

	for method, _ := range ci.controllerInstance().routes {
		if _, err := app.findRoute(ci.Name(), method); err != nil {
			panic(fmt.Errorf("%s.%s: method not found", ci.Name(), method))
		}
	}
}

// https://groups.google.com/forum/?fromgroups=#!search/FileServer$20listing/golang-nuts/bStLPdIVM6w/VKlnMlli5AwJ
func noDirListing(h http.Handler) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//fmt.Printf("noDirListing %s %d\n", r.URL.Path, len(r.URL.Path))
		if len(r.URL.Path) == 0 || strings.HasSuffix(r.URL.Path, "/") {
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}
		h.ServeHTTP(w, r)
	})
}

func (app *Application) setupStaticHandler() {
	staticPrefix := "/" + filepath.Base(app.StaticPath) + "/"
	h := http.StripPrefix(staticPrefix, noDirListing(http.FileServer(http.Dir(app.StaticPath))))
	app.router.PathPrefix(staticPrefix).Handler(h)
}

func interfaceName(in interface{}) string {
	v := reflect.ValueOf(in)
	if v.Kind() != reflect.Ptr || v.Elem().Kind() != reflect.Struct {
		panic(errors.New("interfaceName() argument must be a pointer to struct"))
	}

	return reflect.Indirect(v).Type().Name()
}
