package main

import ( 
	"fmt"
	"github.com/pelletier/go-toml" 
	//"github.com/stvp/go-toml-config"
)

type App struct {
	Name string
	ipPort int
}


func main() {
	app := App{}
	config, _ := toml.LoadFile("app.conf")
	app.Name = config.Get("name").(string)
	x := int(config.Get("port").(int64))
	app.ipPort = int(x)
	fmt.Printf("name = %s %v %d\n", app.Name, config.Get("name"), app.ipPort)
}