package main

import ( 
	"fmt"
	"errors"
	"os"
	mvc "mistoo/movico" 
)

type UsersController struct {
	mvc.Controller
	num int
}

func (c *UsersController) Init() {
	//c.Routes["New"] = mvc.RouteConf{ Method: "GET", Path: "/new/{project_id:[0-9]+}", Name: "user_new" }
	//c.Routes["Show"] = mvc.RouteConf{ Method: "GET", Path: "/{id}", Name: "user_show" }
	c.Routes["Display"] = mvc.RouteConf{ Method: "GET", Path: "/display/{id:[0-9]+}", Name: "display_user" }
	fmt.Printf("INIT CALL\n")
	//u = db.User.Find(id)
	//return c.Render(r)
}


func (c *UsersController) New(req *mvc.Request) (error, string) {
	//fmt.Printf("NEW %+v %+v\n", req, project_id)
	//u = db.User.Find(id)
	return nil, c.Render(req)
	//return nil, ""
}

func (c *UsersController) Create(r *mvc.Request) (err error, content string) {
	//c.App.Log.Debug("Handled index")
	c.num++
	//fmt.Fprintf(this.Response, "Hello World, counter: %d", c.num)
	err = nil
	content = c.Render(r)
	return
}

func (c *UsersController) Index(req *mvc.Request) (err error, content string) {
	//c.App.Log.Debug("Handled index")
	c.num++
	fmt.Sprintf("Index, counter: %d\n", c.num)
	fmt.Printf("%s\n", c.Render(req))
	return nil, "aa"
}

func (c *UsersController) Display(req *mvc.Request, id int) (err error, re *mvc.Response) {
	fmt.Printf("req %+v\n", req)
	
	re = req.Response()
	err = errors.New("AAA")
	panic("ADUPA")
	fmt.Fprintf(re, "Ala ma kota<br><b>Ela</b> ma psa")
	return
}

func (c *UsersController) Show(req *mvc.Request, id int) (err error, re *mvc.Response) {
	fmt.Printf("req %+v\n", req)
	re = req.Response()
	
	if id == 100 {
		re.NotFound()
	} else {
		re.Printf("Req %#v<br />", req.Request)
		re.Printf("Ala ma kota %d<br />", c.num)
		//re.Redirect("http://ipsos.pl")
		re.Dump()
		c.num++
		//content = fmt.Sprintf("Show id=%d, counter: %d", id, c.num)
	}
	
	return
}

func (c *UsersController) ShowAdmins(req *mvc.Request) (err error, content string) {
	//c.App.Log.Debug("Handled index")
	c.num++
	//fmt.Fprintf(this.Response, "Hello World, counter: %d", c.num)
	err = nil
	content = c.Render(req)
	return
}

func (c *UsersController) Profile(req *mvc.Request) (err error, content string) {
	//c.App.Log.Debug("Handled index")
	c.num++
	//fmt.Fprintf(this.Response, "Hello World, counter: %d", c.num)
	err = nil
	content = c.Render(req)
	return
}

func main() {
	t, _ := mvc.NewNativeTemplate("/home/users/mis/go/src/mistoo/movico/example/views/_base.html", "/home/users/mis/go/src/mistoo/movico/example/views/login.html", )
	//fmt.Printf("err %+v\n", t.Compile())
	fmt.Printf("err %+v\n", t.Render(os.Stdout, t))

	if false {

	app := mvc.New()
	app.AddController(&UsersController{}, "/")

	//app.HandleMethod(&uc, "ShowAdmins", "/admins", ) // Handle a simple function
	//app.HandleController(uc, "") // Handle a simple function
	//app.AddRoute("/users/{id}/profile", uc.Profile
	app.Run()
	}
}
