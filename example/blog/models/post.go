package models

type Modeler interface {
	IsNew()
}

type Model struct {
	
}

type Post struct {
    Id int
    Title string	
	Body string
}

func NewPost(title, body string) *Post {
	return &Post{1, title, body}
}

func (p *Post) IsNew() bool {
	return p.Id == 0
}


