package controllers

import (
	"fmt"
	//"errors"
	"sync"
	//"os"
	mvc "mistoo/movico"
	form "mistoo/movico/form"
	//"../models"
)

type Post struct {
	Id         int    `schema:"id"`
	Author     string `schema:"author"`
	Title      string `schema:"title"`
	Body       string `schema:"body"`
}

func (p *Post) ObjectId() string {
	return fmt.Sprintf("%d", p.Id)
}

/*
func (p *Post) ClassName() string {
	return "Post"
}*/

type PostStore struct {
	mutex sync.Mutex
	store []Post
}

func (s *PostStore) Save(p *Post) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	if p.Id == 0 { // new
		p.Id = len(s.store) + 1
		s.store = append(s.store, *p)
		fmt.Printf("Saving post %+v %v\n", p, s.store[p.Id-1]);
	} else {
		s.store[p.Id-1] = *p
	}
	//fmt.Printf("STORE %d %v\n", p.Id, s.store)
}

func (s *PostStore) Find(id int) *Post {
	if id > len(s.store) {
		return nil
	}
	s.mutex.Lock()
	defer s.mutex.Unlock()

	post := s.store[id-1]
	return &post
}

type Posts struct {
	ApplicationController
	postForm *form.Form
	db       *PostStore
}

func (c *Posts) BeforeX(req *mvc.Request) {
	//c.ApplicationController.Before(req)
	c.Log.Trace("Posts Before filter %s", req.MethodFQN)
}

func (c *Posts) Init() {
	c.SetDefaultFormats("html", "json", "xml")
	c.AddRoute("Display", "/display/{id}").Method("GET").Name("display").Formats("json")
	
	f := form.NewFormFor("post")
	f.AddTextField("title")
	f.AddTextField("author")
	f.AddTextArea("body")
	f.AddSubmit("Save")
	c.postForm = f

	c.db = &PostStore{}
	for i := 0; i < 10; i++ {
		c.db.Save(&Post{Title: fmt.Sprintf("Article #%d", i+1), Body: "Lorem ipsum"})
	}
}

// GET /posts
func (c *Posts) Index(req *mvc.Request) *mvc.Response {

	value := 0
	if v, ok := req.Session.Values["dupa"]; ok {
		value = v.(int) + 1
	}
	req.Session.Values["dupa"] = value
	//fmt.Printf("S %s %s %v\n", req.Session.Name(), req.Session.ID, value)
	return c.Render(req, "posts", c.db.store)
}

// GET /posts/{id}
func (c *Posts) Show(req *mvc.Request, id int) *mvc.Response {
	if id == 0 {
		return req.Response().BadRequest("Requested invalid post id (#%d)", id)
	}
	post := c.db.Find(id)
	if post == nil {
		return req.Response().NotFound("Post #%d not found", id)
	}
	panic("Doo:aa:aa")
	return c.Render(req, "post", post)
}

func (c *Posts) Display(req *mvc.Request, id int) *mvc.Response {
	return c.Show(req, id)
}


// GET /posts/new
func (c *Posts) New(req *mvc.Request) *mvc.Response {
	action := c.MethodUrl("Create")
	form := c.postForm.Instance().Action(action).RenderHTML() 
	return c.Render(req, "form", form)
}

// POST /posts
func (c *Posts) Create(req *mvc.Request) *mvc.Response {
	c.Log.Trace("PP %+v", req.Params())
	post := Post{}
	if err := c.DecodeSchema(req, &post); err != nil {
		c.Log.Error("Decoding Post error: %v", err)
		return req.ErrorResponse(err)
	}
	/*
	file, err := req.ModelFile(&post, "attachment")
	if err == nil {
		c.Log.Trace("FILE %+v %+v", file.File.(*os.File).Name(), file.Header.Filename)
	}*/
	c.db.Save(&post)
	return req.RedirectResponse(c.MethodUrl("Show", "id", post.ObjectId()))
}

// GET posts/{id}/edit
func (c *Posts) Edit(req *mvc.Request, id int) *mvc.Response {
	post := c.db.Find(id)
	if post == nil {
		return req.NotFoundResponse()
	}
	post.Author = "J. Doe"
	post_data, _ := c.EncodeSchema(post)
	c.Log.Trace("post_data %+v", post_data)
	action := c.MethodUrl("Update", "id", post.ObjectId())
	form := c.postForm.Instance().Action(action).Method("PUT").SetData(post_data) 

	re := req.Response()
	c.RenderTo(re, "form", form.RenderHTML(), "formo", form, "post", post)
	return re
}

// PUT /posts/{id}
func (c *Posts) Update(req *mvc.Request, id int) *mvc.Response {
	post := c.db.Find(id)
	if post == nil {
		return req.NotFoundResponse()
	}

	if err := c.DecodeSchema(req, post); err != nil {
		panic(err)
	}

	c.db.Save(post)
	return req.RedirectResponse(c.ModelUrl(post))
}

// DELETE /posts/{id}
func (c *Posts) Delete(req *mvc.Request, id int) *mvc.Response {
	post := c.db.Find(id)
	if post == nil {
		return req.NotFoundResponse()
	}
	c.Log.Info("DELETE %+v", post)
	return req.RedirectResponse(c.MethodUrl("Index"))
}

