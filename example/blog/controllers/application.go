package controllers

import ( 
	mvc "mistoo/movico" 
)

type ApplicationController struct {
	mvc.Controller
}

func (c *ApplicationController) Init() {
	c.SetRoutePath("/")
}

func (c *ApplicationController) IsLoggedIn(req *mvc.Request) bool {
	_, ok := req.Session.Values["userId"] 
	return ok
}

func (c *ApplicationController) Before(req *mvc.Request) *mvc.Response {
	c.Log.Trace("Application Before filter %s %s", req.MethodFQN, c.Name())
	c.Log.Debug("Session %+v", req.Session.Values)

	if c.IsLoggedIn(req) {
		return nil
	}
	
	switch {
	case req.MethodFQN == "Posts.Show":
		return nil
	case req.MethodFQN == "Posts.Display":
		return nil
	case req.MethodFQN == "Posts.Index":
		return nil
	case req.ControllerName == "Sessions":
		return nil
	}
	req.Session.Values["back_to"] = req.URL.RequestURI()
	return req.RedirectResponse("/login")
}

func (c *ApplicationController) After(re *mvc.Response) {
	//c.Log.Trace("Application After filter %s.%s", req.ControllerName, req.MethodName)
}

