package controllers

import ( 
	mvc "mistoo/movico" 
)

type Sessions struct {
	ApplicationController
}

func (c *Sessions) Init() {
	c.SetRoutePath("/")
	c.AddRoute("New", "/login").Name("login")
	c.AddRoute("Create", "/login").Method("POST").Name("auth")
	c.AddRoute("Delete", "/logout").Name("logout")
}

func (c *Sessions) New(req *mvc.Request) *mvc.Response {
	if t, ok := req.Session.Values["time"]; !ok {
		req.Session.Values["time"] = 1
	} else {
		req.Session.Values["time"] = t.(int) + 1
	}
	html := c.Render(req)
	return req.Response(html)
}

func (c *Sessions) Create(req *mvc.Request) *mvc.Response {
	p := req.Params()
	c.Log.Trace("Params %v", p)
	c.Log.Trace("Session %v", req.Session)
	
	re := req.Response()
	
	auth := true
	if auth == false {
		req.Session.AddFlash("Auth failed")
		return re.Redirect("/login")
	} 

	req.Session.Values["userId"] = 1

	if u, ok := req.Session.Values["back_to"]; ok {
		re.Redirect(u.(string))
	} else {
		re.Redirect("/")
	}
	
	return re
}

func (c *Sessions) Delete(req *mvc.Request) *mvc.Response {
	return req.Response("Auth")
}
