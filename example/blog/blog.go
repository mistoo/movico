package main

import ( 
	"mistoo/movico" 
	//_ "mistoo/movico/renderer/html"
	//_ "mistoo/movico/renderer/json"
	//_ "mistoo/movico/renderer/xml"
	_ "mistoo/movico/db/mongo" 

	. "./controllers"
)

func main() {
	app := movico.NewApplication()
	app.AddController(&ApplicationController{})
	app.AddController(&Sessions{})
	app.AddController(&Posts{})
	app.AddController(&UsersController{})
	app.SetDefaultRoute("Posts", "Index")
	app.Run()
}
