package movico

import (
	"encoding/json"
	"strings"
	"fmt"
	"github.com/gorilla/schema"
)

type Decoder struct {
	*schema.Decoder
}

func NewDecoder() *Decoder {
	d := new(Decoder)
	d.Decoder = schema.NewDecoder()
	return d
}

func (e *Decoder) Decode(v interface{}, src map[string][]string) error {
	return e.Decoder.Decode(v, src)
}

func (e *Decoder) Encode(v interface{}) (map[string][]string, error) {
	return Marshall(v)
}

func Marshall(v interface{}) (map[string][]string, error) {
	j, err := json.Marshal(v)
	if err != nil {
		return nil, err
	}

	m := make(map[string]interface{})
	if err = json.Unmarshal(j, &m); err != nil {
		return nil, err
	}
	r := make(map[string][]string)
	
	for k, v := range m {
		if sl, ok := v.([]string); ok {
			r[strings.ToLower(k)] = sl
			continue
		} 

		s, ok := v.(string)
		if !ok {
			s = fmt.Sprintf("%v", v)
		}
		r[strings.ToLower(k)] = []string{s}
	}
	return r, nil
}
