package movico

import (
	"bufio"
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"path"
	"path/filepath"
	"strings"
	"text/template"
)

func parseCookie(cookieStr string) (*http.Cookie, error) {
	req, err := http.ReadRequest(bufio.NewReader(strings.NewReader(fmt.Sprintf("GET / HTTP/1.0\r\nCookie: %s\r\n\r\n", cookieStr))))
	if err != nil {
		return nil, err
	}
	cookies := req.Cookies()
	if len(cookies) == 0 {
		return nil, fmt.Errorf("no cookies")
	}
	return cookies[0], nil
}

func createTestConfig(root string, config map[string]string) string {
	file, err := ioutil.TempFile(root, "tmpconfig")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	conf := `
env  = "{{.env}}"
root = "{{.root}}"
prefix = "{{.prefix}}"
name = "{{.name}}"
title = "{{.title}}"
version = "{{.version}}"

listen = "{{.listen}}"
port = "{{.port}}"

foo = "bar"

[development]
version = "{{.version}}dev"

[sessions]
store = "{{.sessionsStore}}"
id = "{{.sessionsId}}"
secret = "{{.sessionsSecret}}"

[development.sessions]
id = "{{.sessionsId}}dev"
`
	t := template.Must(template.New("conf").Parse(conf))

	err = t.Execute(file, config)
	if err != nil {
		panic(err)
	}
	return file.Name()
}

/*
func initViews(c *ControllerInterface) {
	c.
}*/

func initTestApp(testConfig map[string]string) *Application {
	if testConfig == nil {
		testConfig = map[string]string{
			"env":            "production",
			"name":           "application",
			"title":          "Application Title",
			"version":        "111",
			"listen":         "127.0.0.1",
			"port":           "9999",
			"templates":      "native",
			"root":           "",
			"prefix":         "/app",
			"sessionsStore":  "cookie",
			"sessionsId":     "movicoSession",
			"sessionsSecret": "secret",
			"sessionsMaxAge": "0",
		}
	}

	root, err := ioutil.TempDir("", "movicoroot")
	if err != nil {
		panic(err)
	}
	if err := os.Mkdir(root+"/log", 0755); err != nil {
		panic(err)
	}

	if err := os.Mkdir(root+"/views", 0755); err != nil {
		panic(err)
	}

	// true root path
	current, _ := os.Getwd()
	os.Chdir(root)
	wd, _ := os.Getwd()

	testConfig["root"] = wd
	os.Chdir(current)

	configPath := createTestConfig(root, testConfig)
	app := newApplication(configPath, false)
	return app
}

func testHttp(app *Application, uri string, withBody bool) string {
	u := uri
	if !strings.HasPrefix(u, "/") {
		u = path.Join(app.RoutePrefix, uri)
	}
	req, _ := http.NewRequest("GET", u, nil)
	rw := httptest.NewRecorder()
	rw.Body = new(bytes.Buffer)
	app.ServeHTTP(rw, req)

	if withBody {
		return fmt.Sprintf("%s %d %s", uri, rw.Code, rw.Body.String())
	}

	return fmt.Sprintf("%s %d", uri, rw.Code)
}

func createTestTemplate(path, body string) {
	dir := filepath.Dir(path)
	os.MkdirAll(dir, 0755)

	if err := ioutil.WriteFile(path, []byte(body), 0644); err != nil {
		panic(err)
	}
}
