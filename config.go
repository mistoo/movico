package movico

import (
	"fmt"
	"os"
	"path"
	
	getopt "github.com/kesselborn/go-getopt"
	"github.com/pelletier/go-toml"
)

func getConfigValue(tree *toml.TomlTree, name string) string {
	vi := tree.Get(name)
	if vi == nil {
		return ""
	}

	value := ""

	if v, ok := vi.(string); ok {
		value = v
	} else {
		panic(fmt.Errorf("%s: config param must be a string", name))
	}

	return value
}

func getConfigValueEnv(tree *toml.TomlTree, env, name, defaultValue string) string {
	value := ""
	if env == "production" {
		value = getConfigValue(tree, name)
	} else {
		value = getConfigValue(tree, fmt.Sprintf("%s.%s", env, name))
		if value == "" {
			value = getConfigValue(tree, name)
		}
	}
	if value == "" {
		value = defaultValue
	}
	return value
}

func (app *Application) loadConfig(configPath string, verbose bool) {
	if configPath == "" {
		panic(fmt.Errorf("%s: no config file given", configPath))
	}

	p := configPath
	if configPath[0] != '/' {
		p = path.Join(app.RootPath, configPath)
	}
	if verbose {
		fmt.Printf("Loading config %s\n", app.relativePath(p))
	}
	tomlConfig, err := toml.LoadFile(p)
	if err != nil {
		panic(fmt.Errorf("Load config '%s' failed: %v", configPath, err))
	}

	config := make(map[string]string)

	// env first for getConfigValueEnv()
	env := app.argpConfig["env"] // argp
	if env == "" {
		if env = getConfigValue(tomlConfig, "env"); env == "" {
			env = app.defaultConfig["env"]
		}
	}
	config["env"] = env
	//fmt.Printf("XXXENV [%s], [%s], [%s] => %s\n", app.argpConfig["env"], getConfigValue(tomlConfig, "env"), app.defaultConfig["env"], env)

	for name, defaultValue := range app.defaultConfig {
		if name == "env" {
			continue
		}
		v := app.argpConfig[name] // argp
		if v == "" {
			v = getConfigValueEnv(tomlConfig, env, name, defaultValue)
		}
		config[name] = v
	}
	app.config = config

	if config["root"] != "" {
		app.RootPath = path.Clean(config["root"])
	}

	if err := os.Chdir(app.RootPath); err != nil {
		panic(fmt.Errorf("chdir %s failed: %s", app.RootPath, err))
	}

	app.ViewsPath = path.Join(app.RootPath, "views")
	app.StaticPath = path.Join(app.RootPath, "static")
	app.LogPath = path.Join(app.RootPath, "log", fmt.Sprintf("%s.log", app.config["env"]))

	// prefixed app
	if config["prefix"] != "" {
		app.RoutePrefix = path.Join("/", config["prefix"])
		app.router = app.router.PathPrefix(app.RoutePrefix).Subrouter().StrictSlash(true)
	}
	app.StaticPrefix = path.Join("/", app.RoutePrefix, "static")

	// database
	dbConfig := make(map[string]string)
	dbConfigTree := tomlConfig.Get("database")

	if dbConfigTree != nil {
		tree := dbConfigTree.(*toml.TomlTree)

		if env != "production" { // merge configs
			envTree := tomlConfig.Get(fmt.Sprintf("%s.database", env)).(*toml.TomlTree)
			if envTree != nil {
				for _, name := range envTree.Keys() {
					tree.Set(name, getConfigValue(envTree, name))
				}
			}
		}

		for _, name := range tree.Keys() {
			//fmt.Printf("key %s %+v\n", name, dbConfigTree.Get(name))
			dbConfig[name] = getConfigValue(tree, name)
		}
	}
	app.dbConfig = dbConfig
}

func (app *Application) setDefaultConfig() {
	if app.defaultConfig != nil {
		return
	}

	var options = map[string]string{
		"env":              "development",
		"name":             "unnamed",
		"title":            "Unnamed App",
		"version":          "",
		"listen":           "localhost",
		"port":             "3000",
		"templates":        "native",
		"root":             "",
		"prefix":           "",
		"sessions.store":   "cookie",
		"sessions.id":      "movicoSession",
		"sessions.secret":  "secret",
		"sessions.max_age": "0",
	}
	app.defaultConfig = options
	app.config = options
}

func (app *Application) parseArgp() {
	app.setDefaultConfig()
	app.argpConfig = make(map[string]string)

	optionDefinition := getopt.Options{
		"description",
		getopt.Definitions{
			{"env|e|MOVICO_ENV", "Enviroment to run app under", getopt.Optional, []string{"development", "production"}},
			{"root|r", "App root directory", getopt.Optional, "/srv/app"},
			{"config|c", "Config file", getopt.Optional | getopt.ExampleIsDefault, "<root>/app.conf"},
			{"listen|l", "Address to listen to", getopt.Optional | getopt.ExampleIsDefault, "127.0.0.1"},
			{"port|p", "Port to run app on", getopt.Optional | getopt.ExampleIsDefault, 3000},
		},
	}
	options, _, _, err := optionDefinition.ParseCommandLine()
	help, wantsHelp := options["help"]

	if err != nil || wantsHelp {
		ec := 0

		switch {
		case wantsHelp && help.String == "usage":
			fmt.Print(optionDefinition.Usage())
		case wantsHelp && help.String == "help":
			fmt.Print(optionDefinition.Help())
		default:
			fmt.Println("Parse error: ", err.Error(), "\n", optionDefinition.Usage())
			ec = err.ErrorCode
		}
		os.Exit(ec)
	}

	for _, name := range []string{"env", "root", "config", "listen"} {
		if o := options[name]; o.Set && o.String != "" {
			app.argpConfig[name] = o.String
		}
	}

	if o := options["port"]; o.Set {
		app.argpConfig["port"] = fmt.Sprintf("%d", o.Int)
	}

	// set as defaults
	for k, v := range app.argpConfig {
		app.defaultConfig[k] = v
	}
}
