package movico

import (
	"fmt"
	"sync"
)

type named interface {
	Name() string
}

type registry struct {
	name     string
	data      map[string]interface{}
	mutex    sync.Mutex
	frozen   bool
}

func newRegistry(name string) *registry {
	return &registry{name: name, data: make(map[string]interface{})}
}

func (r *registry) freeze() {
	r.mutex.Lock()
	defer r.mutex.Unlock()

	r.frozen = true
}

func (r *registry) get(name string) (o interface{}, ok bool) {
	if !r.frozen {
		r.mutex.Lock()
		defer r.mutex.Unlock()
	}
	o, ok = r.data[name]
	return
}

func (r *registry) set(d named) {
	r.setU(d.Name(), d)
}

func (r *registry) setU(name string, o interface{}) {
	if r.frozen {
		panic(fmt.Errorf("%s: registry is frozen"))
	}
	r.mutex.Lock()
	defer r.mutex.Unlock()
	r.data[name] = o
}
