package movico

import (
	"io"
	//"fmt"
	"html/template"
	"path/filepath"
	//"os"
)

type NativeEngine int8

func (e NativeEngine) Name() string {
	return "native"
}

func (e NativeEngine) Ext() string {
	return ".html"
}

func (e NativeEngine) NewTemplate(layout, template string, funcMap template.FuncMap) (Templater, error) {
	return NewNativeTemplate(layout, template, funcMap)
}

type NativeTemplate struct {
	*template.Template
	name     string
	path     string
	layout   string
	compiled bool
}

func NewNativeTemplate(layout, path string, funcMap template.FuncMap) (*NativeTemplate, error) {
	templateJar.Lock()
	defer templateJar.Unlock()

	name := path

	if t, ok := templateJar.Get(name); ok {
		tt := t.(*NativeTemplate)
		tt.Funcs(funcMap) // overwrite cached map
		return tt, nil
	}

	tname := filepath.Base(layout)
	if layout == "" {
		tname = filepath.Base(path)
	}
	//fmt.Printf("TNAME %s <= (%s,%s)\n", tname, layout, path)
	tmpl := template.New(tname).Funcs(funcMap)

	t := &NativeTemplate{tmpl, name, path, layout, false}
	err := t.Compile()
	if err != nil {
		return nil, err
	}
	t.compiled = true
	//fmt.Printf("TemplateJar.cache = %+v %s %v\n", TemplateJar.cache, name, t)

	templateJar.Set(name, t)
	return t, nil
}

func (t *NativeTemplate) Name() string {
	return t.name
}

func (t *NativeTemplate) Funcs(funcMap template.FuncMap) {
	t.Template.Funcs(funcMap)
}

func (t *NativeTemplate) Clone() *NativeTemplate {
	tc, err := t.Template.Clone()
	if err != nil {
		panic(err)
	}
	return &NativeTemplate{tc, t.name, t.path, t.layout, t.compiled}
}

func (t *NativeTemplate) Compile() error {
	if t.compiled {
		return nil
	}

	if t.layout == "" {
		_, err := t.ParseFiles(t.path)
		return err
	}
	//fmt.Printf("Compiling %s %s\n", t.path, t.layout)
	_, err := t.ParseFiles(t.path, t.layout)
	return err
}

func (t *NativeTemplate) Render(w io.Writer, data interface{}) error {
	return t.Execute(w, data)
}

/*
var funcs = template.FuncMap {
	//"reverse": reverse,
}

func Template(name, path, layout string) *template.Template {
	TemplateJar.mutex.Lock()
	defer TemplateJar.mutex.Unlock()

	if t, ok := TemplateJar.cache[name]; ok {
		return t
	}

	t := template.New(name).Funcs(funcs)
	t = template.Must(t.ParseFiles(
		"TemplateJar/_base.html",
		filepath.Join("TemplateJar", name),
	))
	TemplateJar.cache[name] = t

	return t
}
*/

func init() {
	RegisterTemplateEngine(new(NativeEngine))
}
