package movico

import (
	"github.com/orfjackal/gospec/src/gospec"
	. "github.com/orfjackal/gospec/src/gospec"
	"io/ioutil"
	"net/http"
	"os"
)

func ControllerSpec(c gospec.Context) {
	app := initTestApp(nil)
	al := &Aliases{}
	app.AddController(al)

	mal := &MailAliases{}
	app.AddController(mal)

	fi := &FishController{}
	app.AddController(fi)

	c.Specify("Controller", func() {

		c.Specify("Should have proper Names", func() {
			c.Expect(al.className, Equals, "Aliases")
			c.Expect(al.modelName, Equals, "Alias")
			c.Expect(al.viewsPath, Equals, "aliases")
			c.Expect(al.routePath, Equals, "/aliases")

			c.Expect(mal.className, Equals, "MailAliases")
			c.Expect(mal.modelName, Equals, "MailAlias")
			c.Expect(mal.viewsPath, Equals, "mail_aliases")
			c.Expect(mal.routePath, Equals, "/mail-aliases")

			c.Expect(fi.className, Equals, "FishController")
			c.Expect(fi.viewsPath, Equals, "fish")
			c.Expect(fi.modelName, Equals, "Fish")
		})

		c.Specify("Should be \"mountable\"", func() {
			c.Expect(fi.routePath, Equals, "/foo/bar/fish")

			u := app.Url("FishController", "Show", "id", "123")
			c.Expect(u, Equals, "/app/foo/bar/fish/123")
			c.Expect(testHttp(app, u, true), Equals, u+" 200 Fish id=123")

			c.Expect(app.Url("FishController", "Show", "id", "123", "foo", "true"), Equals, "/app/foo/bar/fish/123?foo=true")
		})

		c.Specify("Should help with URLs", func() {
			c.Expect(al.MethodUrl("Show", "id", "1"), Equals, "/app/aliases/1")
			c.Expect(mal.MethodUrl("Show", "id", "1234"), Equals, "/app/mail-aliases/1234")
			c.Expect(fi.MethodUrl("Show", "id", "1"), Equals, "/app/foo/bar/fish/1")
		})

		c.Specify("Should provide template paths", func() {
			c.Expect(fi.methodTemplatePath("Show"), Equals, "fish/show")
			c.Expect(fi.methodTemplatePath("Index"), Equals, "fish/index")
			c.Expect(al.methodTemplatePath("Show"), Equals, "aliases/show")
		})

		c.Specify("Should render default templates", func() {
			r, _ := http.NewRequest("GET", "/aliases/1234", nil)
			req := app.newRequest(r, "Aliases", "Show", al.methodTemplatePath("Show"))

			os.Mkdir(app.ViewsPath, 0755)
			os.Mkdir(app.ViewsPath+"/aliases", 0755)
			if err := ioutil.WriteFile(app.ViewsPath+"/aliases/show.html", []byte("Fish {{.alias.Id}}"), 0644); err != nil {
				panic(err)
			}

			alias := &Alias{1234, "foo", "bar"}

			re := al.Render(req, "alias", alias)
			c.Expect(re, Not(IsNil))
			c.Expect(re.String(), Equals, "Fish 1234")

			alias.Id = 2345
			re = al.Render(req, "alias", alias)
			c.Expect(re, Not(IsNil))
			c.Expect(re.String(), Equals, "Fish 2345")

			reTo := req.Response()
			al.RenderTo(reTo, "alias", alias)
			c.Expect(reTo.String(), Equals, re.String())
		})

		c.Specify("Should render custom templates", func() {
			r, _ := http.NewRequest("GET", "/aliases/1234", nil)
			req := app.newRequest(r, "Aliases", "Show", al.methodTemplatePath("foo"))

			createTestTemplate(app.ViewsPath+"/aliases/foo.html", "FooFish {{.alias.Id}}")

			alias := &Alias{1234, "foo", "bar"}
			re := al.RenderTemplate(req, "aliases/foo", "alias", alias)
			c.Expect(re, Not(IsNil))
			c.Expect(re.String(), Equals, "FooFish 1234")
		})

		c.Specify("Should render JSON", func() {
			r, _ := http.NewRequest("GET", "/aliases/1234.json", nil)
			req := app.newRequest(r, "Aliases", "Show", "")

			//c.Expect(req.respondsTo("json"), Equals, true)
			reTo := req.Response()
			alias := &Alias{2345, "foo", "bar"}
			al.RenderTo(reTo, "alias", alias)
			c.Expect(reTo.String(), Equals, `{"id":2345,"name":"foo","to":"bar"}`)
		})

		c.Specify("Should render XML", func() {
			r, _ := http.NewRequest("GET", "/aliases/1234.xml", nil)
			req := app.newRequest(r, "Aliases", "Show", "")

			//c.Expect(req.respondsTo("json"), Equals, true)
			reTo := req.Response()
			alias := &Alias{2345, "foo", "bar"}
			al.RenderTo(reTo, "alias", alias)
			c.Expect(reTo.String(), Equals, `<Alias id="2345"><name>foo</name><to>bar</to></Alias>`)
		})
	})
}
