package movico

import (
	"io"
	//"fmt"
	"github.com/flosch/pongo"
)

type PongoEngine int8

func (e PongoEngine) Name() string {
	return "pongo"
}

func (e PongoEngine) Ext() string {
	return ".html"
}

func (e PongoEngine) NewTemplate(layout, template string) (Templater, error) {
	return NewPongoTemplate(layout, template)
}


type PongoTemplate struct {
	*pongo.Template
	name     string
	path     string
	layout   string
	compiled bool
}

func NewPongoTemplate(layout, path string) (*PongoTemplate, error) {
	templateJar.Lock()
	defer templateJar.Unlock()

	name := path

	if t, ok := templateJar.Get(name); ok {
		tt := t.(*PongoTemplate)
		return tt, nil
	}

	p, err := pongo.FromFile(path, nil)
	if err != nil {
		return nil, err
	}
	
	t := &PongoTemplate{p, name, path, layout, false}
	t.compiled = true
	//fmt.Printf("TemplateJar.cache = %+v %s %v\n", TemplateJar.cache, name, t)

	templateJar.Set(name, t)
	return t, nil
}

func (t *PongoTemplate) Name() string {
	return t.name
}

func (t *PongoTemplate) Compile() error {
	return nil
}

func (t *PongoTemplate) Render(w io.Writer, data interface{}) error {
	out, err := t.Execute(&pongo.Context{"data": data})
	if err == nil {
		w.Write([]byte(*out))
		return nil
	} 
	return err
}
