package main

import "fmt"

type IController interface {
	Index(string)
	Show(string)
}

type App struct {
	bar string
}

func (a *App) Show(s string) {
	fmt.Println("App Show", s)
}

func (a *App) Index(s string) {
	fmt.Println("App Index", s)
}


type Bar struct{
	App
}


func (a *Bar) Show(s string) {
	fmt.Println("Bar Show", s)
}



func Add(f IController) {
	ff := f.Show
	ff("aaa")
}

func main() {
	b := &Bar{}
	Add(b)
}