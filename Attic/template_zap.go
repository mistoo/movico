package movico

import (
	"io"
	"fmt"
	"html/template"
	"path/filepath"
	//"os"
)

type ZapEngine int8

func (e ZapEngine) Name() string {
	return "native"
}

func (e ZapEngine) Ext() string {
	return ".html"
}

func (e ZapEngine) NewTemplate(layout, template string) (Templater, error) {
	return NewZapTemplate(layout, template)
}


type ZapTemplate struct {
	*template.Template
	name     string
	path     string
	layout   string
	compiled bool
}



func NewZapTemplate(layout, path string) (*ZapTemplate, error) {
	templateJar.Lock()
	defer templateJar.Unlock()

	name := path

	if t, ok := templateJar.Get(name); ok {
		tt := t.(*ZapTemplate)
		return tt, nil
	}

	t := &ZapTemplate{template.New(filepath.Base(layout)), name, path, layout, false}
	err := t.Compile()
	if err != nil {
		return nil, err
	}
	t.compiled = true
	//fmt.Printf("TemplateJar.cache = %+v %s %v\n", TemplateJar.cache, name, t)

	templateJar.Set(name, t)
	return t, nil
}

func (t *ZapTemplate) Name() string {
	return t.name
}

func (t *ZapTemplate) Compile() error {
	if t.compiled {
		return nil
	}
	fmt.Printf("Compile %s %s\n", t.path, t.layout)
	_, err := t.ParseFiles(t.layout, t.path)
	return err
}

func (t *ZapTemplate) Render(w io.Writer, data interface{}) error {
	return t.Execute(w, data)
}

/*
var funcs = template.FuncMap {
	//"reverse": reverse,
}

func Template(name, path, layout string) *template.Template {
	TemplateJar.mutex.Lock()
	defer TemplateJar.mutex.Unlock()

	if t, ok := TemplateJar.cache[name]; ok {
		return t
	}

	t := template.New(name).Funcs(funcs)
	t = template.Must(t.ParseFiles(
		"TemplateJar/_base.html",
		filepath.Join("TemplateJar", name),
	))
	TemplateJar.cache[name] = t

	return t
}
*/

