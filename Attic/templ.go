package main

import (
	"os"
	//"fmt"
	"html/template"
)

func Template(name string) *template.Template {
	t := template.New("show.html") // #.Funcs(funcs)
	t = template.Must(t.ParseFiles("example/blog/views/posts/show.html", "example/blog/views/layout/application.html"))
	

	return t
}

func main() {
	t := Template("Foo")
	if err := t.Execute(os.Stdout, t); err != nil {
		panic(err);
	}
}

