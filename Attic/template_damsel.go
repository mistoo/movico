package movico

import (
	"io"
	//"fmt"
	"github.com/dskinner/damsel"
)

type DamselEngine int8

func (e DamselEngine) Name() string {
	return "damsel"
}

func (e DamselEngine) Ext() string {
	return ".dmsl"
}

func (e DamselEngine) NewTemplate(layout, template string) (Templater, error) {
	return NewDamselTemplate(layout, template)
}


type DamselTemplate struct {
	*damsel.Template
	name     string
	path     string
	layout   string
	compiled bool
}

func NewDamselTemplate(layout, path string) (*DamselTemplate, error) {
	templateJar.Lock()
	defer templateJar.Unlock()

	name := path

	if t, ok := templateJar.Get(name); ok {
		tt := t.(*DamselTemplate)
		return tt, nil
	}

	p, err := damsel.ParseFile(path)
	if err != nil {
		return nil, err
	}
	
	t := &DamselTemplate{p, name, path, layout, false}
	t.compiled = true
	//fmt.Printf("TemplateJar.cache = %+v %s %v\n", TemplateJar.cache, name, t)

	templateJar.Set(name, t)
	return t, nil
}

func (t *DamselTemplate) Name() string {
	return t.name
}

func (t *DamselTemplate) Compile() error {
	return nil
}

func (t *DamselTemplate) Render(w io.Writer, data interface{}) error {
	out, err := t.Execute(data)
	if err == nil {
		w.Write([]byte(out))
		return nil
	} 
	return err
}
