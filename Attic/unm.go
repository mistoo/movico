package main

import (
	"net/url"
	"encoding/json"
	"strings"
	"fmt"
)

type Foo struct {
	Int int
	Float int
	String string
	Bool bool
}

func structToUrlValues(v interface{}) (url.Values, error) {
	j, err := json.Marshal(v)
	if err != nil {
		return nil, err
	}
	
	fmt.Printf("mm %v %v\n", j, err)
	m := make(map[string]interface{})
	if err = json.Unmarshal(j, &m); err != nil {
		return nil, err
	}
	r := make(url.Values)
	
	for k, v := range m {
		if sl, ok := v.([]string); ok {
			r[strings.ToLower(k)] = sl
			continue
		} 

		s, ok := v.(string)
		if !ok {
			s = fmt.Sprintf("%v", v)
		}
		r[strings.ToLower(k)] = []string{s}
	}
	return r, nil
}

func main() {
	f := &Foo{1, 2, "3a3", true}
	
	m, err := structToUrlValues(f)
	fmt.Printf("mm %v %v\n", m, err)
}
