package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
)

type Post struct {
	Id         int    `json:"id" xml:"id,attr"`
	Author     string `schema:"author" json:"author" xml:"author"`
	Title      string `schema:"title" json:"title" xml:"title"`
	Body       string `schema:"body" json:"body" xml:"body"`
}

type XmlPost struct { 
	XMLName xml.Name `xml:"post"`
	Post 
}


func main() {
	//j := `{"post":{"Id":1,"Author":"33","Title":"Article #1","Body":"Lorem ipsum"}}`

	j := `{"Id":1,"Author":"Jan B.","Title":"Article #1","Body":"Lorem ipsum", "Foo": "Bar"}`
	//m := make(map[string]string)

	p := &Post{}
	err := json.Unmarshal([]byte(j), p)

	jj, err := json.Marshal(p)
	jjj, err := xml.Marshal(&XmlPost{Post: *p})
	fmt.Printf("FF %v %+v %s %s\n", err, p, jj, jjj)
}
