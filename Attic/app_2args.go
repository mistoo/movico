package movico

import (
	"reflect"
	"net"
	"net/http"
	"fmt"
	"os"
	"strings"
	"strconv"
	"regexp"
	"errors"
	"path"
	"path/filepath"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"github.com/gorilla/sessions"
	"github.com/pelletier/go-toml" 
	"bitbucket.org/pkg/inflect"
)

type Application struct {
	router     *mux.Router
	RoutePrefix  string			// route prefix (path prefix)
	StaticPrefix string         // /static

	// application paths
	RootPath   string			// app root directory
	StaticPath string           // static files directory
	ViewsPath  string           // template root path
	LogPath    string           // log directory

	sessionStore sessions.Store
	tmplEngine   TemplateEngine
	db           Database

	schemaDecoder  *schema.Decoder

	config             map[string]string
	dbConfig           map[string]string


	controllers        map[string]ControllerInterface
	routes             map[string]*mux.Route
	errorTemplates     map[int]string

	Log      *Logger
	listener net.Listener
}

func (app *Application) newRequest(r *http.Request, controllerName, actionName, templatePath string) *Request {
	session, _ := app.sessionStore.Get(r, app.config["sessions.id"])

	req := NewRequest(r, controllerName, actionName, templatePath)
	req.Session = session
	req.DbSession = app.db.Open()
	req.app = app
	return req
}

func (app *Application) wrapRequest(r *http.Request, controllerName, actionName, templatePath string) *Request {
	session, _ := app.sessionStore.Get(r, app.config["sessions.id"])

	req := NewRequest(r, controllerName, actionName, templatePath)
	req.Session = session
	req.DbSession = app.db.Open()
	req.app = app
	return req
}


func (app *Application) deleteRequest(req *Request) {
	if req.DbSession != nil {
		req.DbSession.Close()
	}
	req.app = nil
	req.Session = nil
	req.DbSession = nil
}


func (app *Application) rootPath() string {
	if app.RootPath == "" {
		p := path.Clean(os.Args[0])
		if strings.HasPrefix(p, "/") {
			app.RootPath = filepath.Dir(p)
		} else {
			wd, _ := os.Getwd()
			app.RootPath = filepath.Dir(path.Join(wd, p))
		}
	}
	return app.RootPath
}

func (app *Application) relativePath(path string) string {
	return strings.Replace(path, app.RootPath, ".", 1)
}

func New() *Application {
	app := Application{router: mux.NewRouter()}
	app.controllers = make(map[string]ControllerInterface)
	app.routes = make(map[string]*mux.Route)
	app.errorTemplates = make(map[int]string)
	app.schemaDecoder = schema.NewDecoder()

	fmt.Printf("Movico root at %s\n", app.rootPath())

	app.loadConfig("app.conf")

	app.Log = newLogger()

	if app.config["env"] != "production" {
		app.Log.AddOutput(os.Stdout, "trace")
	} else {
		fmt.Printf("Logging to %s\n", app.relativePath(app.LogPath))
		file, err := os.Open(app.LogPath)
		if err != nil { panic(fmt.Errorf("Cannot open %s: %s", app.LogPath, err)) }
		app.Log.AddOutput(file, "info")
	}

	if app.config["templates"] != "" {
		engine, err := getTemplateEngine(app.config["templates"])
		if err != nil { panic(err) }
		app.tmplEngine = engine
	}

	if (app.config["sessions.store"] != "") {
		s, err := getSessionStore(app.config["sessions.store"])
		if err != nil { panic(err) }
		app.sessionStore, err = s.NewStore(app.config)
		if err != nil { panic(err) }
	}

	if app.dbConfig["adapter"] != "" {
		dbe, err := getDatabaseEngine(app.dbConfig["adapter"])
		if err != nil { panic(err) }

		app.db = dbe.NewEngine()
		err = app.db.Connect(app.dbConfig)
		if err != nil { panic(err) }
		app.Log.Info("Connected to database %#v", app.dbConfig)
	}

	app.setupErrorHandlers()
	
	return &app
}

func (app *Application) setupErrorHandlers() {
	files, err := filepath.Glob(path.Join(app.ViewsPath, "errors/*.html"))
	if err != nil {
		panic(err)
		return
	}

	errorNames := map[string]int {
		"not_found": http.StatusNotFound,
		"internal_server_error": http.StatusNotFound,
		"forbidden": http.StatusForbidden,
		"default": 0,

	}

	for i := range files {
		name := strings.Replace(filepath.Base(files[i]), ".html", "", -1)
		status, ok := errorNames[name]
		if !ok {
			app.Log.Error("%s: unknown error template, skipped", app.relativePath(files[i]))
			continue
		}
		app.errorTemplates[status] = files[i]
	}
}
	

func getConfigValue(tree *toml.TomlTree, name string) string {
	vi := tree.Get(name)
	if vi == nil {
		return ""
	}

	value := ""

	if v, ok := vi.(string); ok {
		value = v
	} else {
		panic(fmt.Errorf("%s: config param must be a string", name))
	}

	return value
}

func getConfigValueEnv(tree *toml.TomlTree, env, name, defaultValue string) string {
	value := ""
	if env == "production" {
		value = getConfigValue(tree, name)
	} else {
		value = getConfigValue(tree, fmt.Sprintf("%s.%s", env, name))
		if value == "" {
			value = getConfigValue(tree, name)
		}
	}
	if value == "" {
		value = defaultValue
	}
	return value
}

func (app *Application) loadConfig(configPath string) {
	p := path.Join(app.RootPath, configPath)

	fmt.Printf("Loading config %s\n", app.relativePath(p))
	tomlConfig, err := toml.LoadFile(p)
	if err != nil {
		panic(fmt.Errorf("Load config '%s' failed: %v", configPath, err))
	}

	var options = map[string]string {
		"env":   "development",
		"name":  "Unnamed",
		"title": "Unnamed App",
		"listen": "localhost",
		"port": "3000",
		"templates": "native",
		"root": "",
		"prefix": "",
		"sessions.store": "cookie",
		"sessions.id": "movicoSession",
		"sessions.secret": "ala ma kota",
		"sessions.max_age": "0",
	}
	env := getConfigValue(tomlConfig, "env")
	if env == "" { 
		env = options["env"] 
	}
	
	config := make(map[string]string)
	for name, defaultValue := range options {
		config[name] = getConfigValueEnv(tomlConfig, env, name, defaultValue)
	}
	app.config = config

	if config["root"] != "" {
		app.RootPath = config["root"]
	}

	if err := os.Chdir(app.RootPath); err != nil {
		panic(fmt.Errorf("chdir %s failed: %s", app.RootPath, err))
	}

	app.ViewsPath = path.Join(app.RootPath, "views")
	app.StaticPath = path.Join(app.RootPath, "static")
	app.LogPath = path.Join(app.RootPath, "log", fmt.Sprintf("%s.log", app.config["env"]))

	// prefixed app
	if config["prefix"] != "" {
		app.RoutePrefix = path.Join("/", config["prefix"])
	    app.router = app.router.PathPrefix(app.RoutePrefix).Subrouter()
	}
	app.StaticPrefix = path.Join("/", app.RoutePrefix, "static")

	// database
	dbConfig := make(map[string]string)
	dbConfigTree := tomlConfig.Get("database").(*toml.TomlTree)

	if dbConfigTree != nil {
		if env != "production" { // merge configs
			envTree := tomlConfig.Get(fmt.Sprintf("%s.database", env)).(*toml.TomlTree)
			if envTree != nil {
				for _, name := range envTree.Keys() {
					dbConfigTree.Set(name, getConfigValue(envTree, name))
				}
			}
		}

		for _, name := range dbConfigTree.Keys() {
			//fmt.Printf("key %s %+v\n", name, dbConfigTree.Get(name))
			dbConfig[name] = getConfigValue(dbConfigTree, name)
		}
	}
	app.dbConfig = dbConfig
	
}

func (app *Application) ModelUrl(model Modeler) string {
	v := reflect.ValueOf(model)
	if v.Kind() != reflect.Ptr || v.Elem().Kind() != reflect.Struct {
		panic(errors.New("ModelUrl: Modeler must be a pointer to struct"))
	}

	controllerName := inflect.Pluralize(reflect.Indirect(v).Type().Name())
	
	methodFQN := fmt.Sprintf("%s.Show", controllerName)
	route, ok := app.routes[methodFQN]
	if !ok {
		panic(fmt.Errorf("%s: route not found", methodFQN))
	}
	
	u, err := route.URL("id", model.ObjectId())
	if err != nil {
		panic(err)
	}

	return u.String()
}


// passing to mux
func (app *Application) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	app.Log.Info("Request %s %s", r.Method, r.URL.Path)
	app.router.ServeHTTP(w, r)
}

func (c *Controller) handleException(re *Response, w http.ResponseWriter) { // non 200 responses
	//if re.Code() == http.StatusUnauthorized
}

func (app *Application) HandleException(code int, toController, toMethod string) { // non 200 responses
	route := app.findRoute(toController, toMethod)
	if route == nil {
		//return fmt.Errorf("%s.%s: route not found", toController, toMethod)
	}

	//u, _ := route.URL()
	
	/*app.exceptionHandlers[code] = func(re *Response, w http.ResponseWriter) {
		//re.Redirect(		
		
	}*/
	//app.Router.Path("/").Handler(route.GetHandler()).Methods("GET")	
	//app.Log.Trace("GET / routed to %s.%s", toController, toMethod)
	//if re.Code() == http.StatusUnauthorized
	
}

func extractPathArgs(path string) []string {
	re := regexp.MustCompile("/{([^}]+)}")

	matches := re.FindAllStringSubmatch(path, -1)
	vars := make([]string, 0, len(matches))
	//fmt.Printf("xx %s %d\n", path, len(vars))
	for _, m := range matches {
	//	fmt.Printf("%s %d, %+v\n", path, i, m[1])
		vars = append(vars, m[1])
	}
	//fmt.Printf("%s %d\n", path, len(vars))
	return vars
}

/* 
 Route path variables are passed as arguments
  foos/                -> ControllerFoo.Index(* Request)  // no params
  foos/{page}          -> ControllerFoo.Index(* Request, page int)
  foos/{id}/edit/{bar} -> ControllerFoo.Index(* Request, id int, bar string)
*/
func verifyActionMethod(method reflect.Value, pathVars []string) ([]reflect.Kind, error) {
	t := method.Type()

	// Verify action function outcomes
	nouts := t.NumOut()
	outErr := errors.New("method must return two parameters: (string, error) or (*movico.Response, error)") 
	if nouts != 2 {
		return nil, outErr
	}

	out := t.Out(0)				// first must be a string or *Response
	if out != reflect.TypeOf((*string)(nil)).Elem() && out != reflect.TypeOf((*Response)(nil)) {
		return nil, outErr
	}

	out = t.Out(1)				// second must be an error
	if out != reflect.TypeOf((*error)(nil)).Elem() {
		return nil, outErr
	}

	// Verify action function arguments
	nargs := t.NumIn()

	ferr := errors.New("method first argument must be *movico.Request") 
	if nargs == 0 {
		return nil, errors.New("method must have at least one *movico.Request argument") 
	}

	in := t.In(0)
	if in.Kind() != reflect.Ptr {
		return nil, ferr
	}

	if reflect.Type(in) != reflect.TypeOf((*Request)(nil)) {
		return nil, ferr
	}

	if len(pathVars) != nargs - 1 {
		return nil, fmt.Errorf("method arguments must be the same as specified in route path: %v", pathVars) 
	}

	argtypes := make([]reflect.Kind, 0, nargs - 1)

	for i := 1; i < nargs; i++ {
        in = t.In(i)
		argtypes = append(argtypes, in.Kind())
	}

	return argtypes, nil
}

func (app *Application) callBeforeActionFilter(method reflect.Value, req *Request) (*Response) {
	if (method == reflect.Value{}) {
		return nil
	}
	
	in := make([]reflect.Value, 1)
	in[0] = reflect.ValueOf(req)
	rv := method.Call(in)

	if len(rv) == 0 {
		return nil
	}
	iface := rv[0].Interface()
	if re, ok := iface.(*Response); ok {
		return re
	} else {
		panic(fmt.Errorf("Action filter %s returns non-movico.Response"))
	}
	return nil
}

func (app *Application) callAfterActionFilter(method reflect.Value, re *Response) {
	if (method == reflect.Value{}) {
		return
	}
	
	in := make([]reflect.Value, 1)
	in[0] = reflect.ValueOf(re)
	method.Call(in)
}


func (app *Application) doCallAction(method reflect.Value, methodArgTypes []reflect.Kind, req *Request) ([]reflect.Value, error) {
	in := make([]reflect.Value, len(methodArgTypes) + 1)
	in[0] = reflect.ValueOf(req)
	n := 1
	
	for _, v := range mux.Vars(req.Request) {
		kind := methodArgTypes[n - 1]
		
		varg := reflect.Value{}
		
		switch kind {
			case reflect.Ptr:
				varg = reflect.ValueOf(nil)
				
			case reflect.Int, reflect.Uint:
			vi, _ := strconv.Atoi(v)
			varg = reflect.ValueOf(vi)
			
		case reflect.String:
			varg = reflect.ValueOf(v)
			
		default:
			app.Log.Error("%s: invalid type %s", v, kind) 
			return nil, fmt.Errorf("%s: invalid type %s", v, kind)
		}
		
		if (varg != reflect.Value{}) {
			in[n] = varg
		}
		n += 1
	}

	rv := method.Call(in)
	if len(rv) != 1 {
		return nil, fmt.Errorf("%s(): Wrong number (%d) of parameters returned, need %d", req.ActionFQN, len(rv), 1)
	}

	return rv, nil
}

func (app *Application) callAction(method reflect.Value, methodArgTypes []reflect.Kind, req *Request) *Response {
	var re *Response = nil

 	rv, err := app.doCallAction(method, methodArgTypes, req)
	
	if err != nil {
		panic(err)
		re = req.ErrorResponse(err)
		
	} else if err, ok := rv[1].Interface().(error); ok && err != nil {
		panic(err)
		re = req.ErrorResponse(err)

	} else if content, ok := rv[0].Interface().(string); ok {
		re = req.Response(content)

	} else if response, ok := rv[0].Interface().(*Response); ok {
		re = response
	}

	if re.Code() > 299 {
		if t, ok := app.errorTemplates[re.Code()]; ok {
			fmt.Printf("FOUND TEMPLATE %d %s\n", re.Code(), t)
			error := errors.New(re.String())
			re.Buffer.Reset()
			app.renderErrorTemplate(re, t, error)
		}
	}
	
	return re
}

func (app *Application) renderErrorTemplate(re *Response, templatePath string, error error) {
	defer app.defaultErrorHandler(re, re.req, error)

	v, err := app.newView(re.req, "", templatePath)
	if err != nil {
		panic(fmt.Errorf("%s: Initialization error: %s", templatePath, err))
	}
	
	err = v.render(re, "error", error, "backtrace", app.backtraceHtml())

	if err != nil {
		panic(fmt.Errorf("%s: render error: %s", templatePath, err))
	}
}

func (app *Application) defaultErrorHandler(w http.ResponseWriter, req *Request, err error) {
	r := recover()
	if r == nil {
		return
	}

	app.Log.Error("Double panic: %+v", r)
	app.Log.Error("Panic: %+v", err)
	
	re := req.Response().Error500(err)
	re.Printf("<br />")
	re.Printf("%s", app.backtraceHtml()) 
	re.Apply(w)
}

func (app *Application) templatedErrorHandler(w http.ResponseWriter, req *Request) {
	r := recover()
	if r == nil {
		return
	}

	app.Log.Error("Panic: %+v", r)
	re := req.Response()

	tpath, ok := app.errorTemplates[re.Code()]
	if !ok {
		tpath, ok = app.errorTemplates[0] // default
	} 

	if !ok {					// no error template
		app.defaultErrorHandler(w, req, r.(error))
		return
	}
	app.renderErrorTemplate(re, tpath, r.(error))
	re.Apply(w)
}


func (app *Application) makeHandlerFunc(methodName string, ci ControllerInterface, routePath string) http.HandlerFunc {
	method := reflect.ValueOf(ci).MethodByName(methodName)
	if (method == reflect.Value{}) {
		panic(fmt.Sprintf("%s.%s: method not found", ci.ClassName(), methodName))
	}

	templatePath := path.Join(ci.controllerInstance().pluralName, strings.ToLower(methodName))
	pathVars := extractPathArgs(routePath)
	methodArgTypes, err := verifyActionMethod(method, pathVars)

	if err != nil {
		panic(fmt.Sprintf("%s.%s: %s", ci.ClassName(), methodName, err))
	}

	before := reflect.ValueOf(ci).MethodByName("Before")
	after := reflect.ValueOf(ci).MethodByName("After")
	
	h := func(w http.ResponseWriter, r *http.Request) {
		req := app.newRequest(r, ci.ClassName(), methodName, templatePath)
		defer app.deleteRequest(req)

		app.Log.Info("  routed to %s()", req.ActionFQN)
		app.Log.Info("  params %+v %+v", req.Params())
		if req.Session != nil {
			app.Log.Info("  session %+v", req.Session)
		}

		defer app.templatedErrorHandler(w, req)
		t0 := time.Now()

		re := app.callBeforeActionFilter(before, req)

		if re == nil {	// call action unless Before() return Response
			re = app.callAction(method, methodArgTypes, req)
		}

		if err := re.saveSession(); err != nil {
			panic(err)
		}
		
		if !re.IsError() {
			app.callAfterActionFilter(after, re)
		}
		app.Log.LogResponse(logLevelInfo, re)
		re.Apply(w)
		app.Log.Info("%s in %v", req.ActionFQN, time.Now().Sub(t0))
	}

	return h
}

func (app *Application) initController(ci ControllerInterface) {
	vci := reflect.ValueOf(ci)

	controllerName := reflect.Indirect(vci).Type().Name()
	ci.controllerInstance().initializeController(controllerName, app)
/*
	m := vci.MethodByName("InitializeController")
	in := make([]reflect.Value, 2)
	in[0] = reflect.ValueOf(t.Name())
	in[1] = reflect.ValueOf(app)
	m.Call(in)
*/

	// call Init() if any 
	initm := vci.MethodByName("Init")
	if (initm != reflect.Value{}) {
		in := make([]reflect.Value, 0)
		initm.Call(in)
	}

	app.controllers[ci.ClassName()] = ci
}

func (app *Application) AddController(ci ControllerInterface) {
	app.initController(ci)

	prefix := path.Join("/", ci.RoutePath())
	if app.RoutePrefix != "" {
		prefix = path.Join("/", app.RoutePrefix, prefix)
	}
	ci.controllerInstance().setRouter(app.router.PathPrefix(prefix).Subrouter())
	app.HandleResource(ci)
}

func (app *Application) findRoute(toController, toMethod string) *mux.Route {
	ci := app.controllers[toController]
	methodFQN := fmt.Sprintf("%s.%s", ci.ClassName(), toMethod)
	if route, ok := app.routes[methodFQN]; ok {
		return route
	}
	return nil
}

/*
func (app *Application) findControllerRoute(ci ControllerInterface, toMethod string) *mux.Route {
	methodFQN := fmt.Sprintf("%s.%s", ci.ClassName(), toMethod)
	if route, ok := app.routes[methodFQN]; ok {
		return route
	}
	return nil
}*/

func (app *Application) SetDefaultRoute(toController, toMethod string) error {
	route := app.findRoute(toController, toMethod)
	if route == nil {
		return fmt.Errorf("%s.%s: route not found", toController, toMethod)
	}

	defaultRoute := app.router.Path("/").Handler(route.GetHandler()).Methods("GET")	
	app.Log.Trace("GET / routed to %s.%s", toController, toMethod)
	
	return defaultRoute.GetError()
}

func applyRouteConf(route *mux.Route, rconf *RouteConf) error {
	if rconf.Method != "" {
		route.Methods(rconf.Method)
	}
	
	if rconf.Name != "" {
		route.Name(rconf.Name)
	}
	
	if rconf.Scheme != "" {
		route.Schemes(rconf.Scheme)
	}

	if rconf.Host != "" {
		route.Host(rconf.Host)
	}

	if len(rconf.Queries) > 0 {
		route.Queries(rconf.Queries...)
	}

	return route.GetError()
}

func (app *Application) registerRoute(cm string, route *mux.Route) {
	app.routes[cm] = route
}

func (app *Application) HandleResource(ci ControllerInterface) {
	app.Log.Debug("%s routes (%s)", ci.ClassName(), ci.RoutePath())

	router := ci.controllerInstance().Router()
	if router == nil {
		panic(fmt.Errorf("%s: Controller not initialized", ci.ClassName()))
	}

	t := reflect.TypeOf(ci)
	for i := 0; i < t.NumMethod(); i++ {
		method := t.Method(i)
		name := method.Name
		
		rconf := ci.controllerInstance().routeConfFor(name)
		if rconf == nil {
			continue
		}
		
		methodFQN := fmt.Sprintf("%s.%s", ci.ClassName(), name)

		if rconf.Path == "" {
			panic(fmt.Errorf("Empty route path for %s", methodFQN))
		}
		
		handler := app.makeHandlerFunc(name, ci, rconf.Path)

		if rconf.Path == "/" { // add extra route without trailing slash
			//app.Log.Trace("  %s %s routed to %s", rconf.Method, ci.RoutePath(), methodFQN)
			route := app.router.Path(ci.RoutePath()).HandlerFunc(handler)
			if err := applyRouteConf(route, rconf); err != nil {
				panic(err)
			}
		}

		route := router.Path(path.Join("/", rconf.Path)).HandlerFunc(handler)
		if err := applyRouteConf(route, rconf); err != nil {
			panic(err)
		}
		app.registerRoute(methodFQN, route)

		//u, _ := route.URL("id", "1")
		app.Log.Trace("  %s %s routed to %s", rconf.Method, path.Join(ci.RoutePath(), rconf.Path), methodFQN)
		//app.Log.Trace("  rev %s => %+v", rconf.Path, u)
	}
}

// https://groups.google.com/forum/?fromgroups=#!search/FileServer$20listing/golang-nuts/bStLPdIVM6w/VKlnMlli5AwJ
func noDirListing(h http.Handler) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//fmt.Printf("noDirListing %s %d\n", r.URL.Path, len(r.URL.Path))
		if len(r.URL.Path) == 0 || strings.HasSuffix(r.URL.Path, "/") {
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}
		h.ServeHTTP(w, r)
	})
}

func (app *Application) Run() {
	u := fmt.Sprintf("%s:%s", app.config["listen"], app.config["port"])
	l, err := net.Listen("tcp", u)
	if err != nil {
		panic("Listen error: " + err.Error())
	}
	app.listener = l

	staticPrefix := "/" + filepath.Base(app.StaticPath) + "/"
	h := http.StripPrefix(staticPrefix, noDirListing(http.FileServer(http.Dir(app.StaticPath))))
	app.router.PathPrefix(staticPrefix).Handler(h)

	app.Log.Debug("Starting HTTP %s", u)
	http.Serve(l, app)
}

// Stops web server.
func (app *Application) Stop() {
	app.listener.Close()
	app.Log.Info("Server stopped")
}

func interfaceClassName(in interface{}) string {
	v := reflect.ValueOf(in)
	if v.Kind() != reflect.Ptr || v.Elem().Kind() != reflect.Struct {
		panic(errors.New("AddModel: Modeler must be a pointer to struct"))
	}
		
	return reflect.Indirect(v).Type().Name()
}
