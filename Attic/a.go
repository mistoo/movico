package main

import (
	"fmt"
	"reflect"
	"os"
	"strings"
	"encoding/json"
	"bitbucket.org/pkg/inflect"
	"labix.org/v2/mgo"
	"labix.org/v2/mgo/bson"
)

type MongoConnection struct {
	Session  *mgo.Session
	Database *mgo.Database
	Url      string
	DbName   string
}

var DBC *MongoConnection = nil
var DB  *mgo.Database = nil

func ConnectMongo(url, database string) *MongoConnection {
	session, err := mgo.Dial(url)
	if err != nil {
		return nil
	}
	session.SetMode(mgo.Monotonic, true)

	db := session.DB(database)
	dbc := new(MongoConnection)
	dbc.Session = session
	dbc.Database = db
	dbc.Url = url
	dbc.DbName = database
	//for _, m := range Models {
	//	RegisterIndexes(m)
	//}

	fmt.Printf("Connected to mongodb on %s, using \"%s\"\n", url, database)
	return dbc
}

func (dbc *MongoConnection) Close() {
	dbc.Session.Close()
	dbc.Database = nil
	dbc.Session = nil
}


type Modeli interface {
	Indexes() []*mgo.Index
}

func toCollectionName(f interface{}) string {
	t := reflect.TypeOf(f)
	for {
		c := false
		switch t.Kind() {
		case reflect.Array, reflect.Chan, reflect.Map, reflect.Ptr, reflect.Slice:
			t = t.Elem()
			c = true
		}
		if !c {
			break
		}
	}
	fmt.Printf("toCollectionName %+v -> %s\n", f, t.Name());
	return inflect.Tableize(t.Name())
}

func callModelMethod(f interface{}, methodName string, isPrefix bool) error {
	t := reflect.TypeOf(f)
	for i := 0; i < t.NumMethod(); i++ {
		method := t.Method(i)
		if (isPrefix && strings.HasPrefix(method.Name, methodName)) || (!isPrefix && method.Name == methodName) {
			ft := method.Func.Type()
			if ft.NumOut() == 1 && ft.NumIn() == 1 {
				v := reflect.ValueOf(f).Method(i).Call([]reflect.Value{})
				if err, ok := v[0].Interface().(error); ok {
					return err
				}
			}
		}
	}
	return nil
}

func Save(m interface{}) error {
	collection := toCollectionName(m)
	c := DB.C(collection)
	id_field := reflect.ValueOf(m).Elem().FieldByName("Id")
	fmt.Printf("id_field %+v %+v\n", bson.ObjectId(id_field.String()), reflect.TypeOf(id_field))

	if id_field.String() == "" {
		id_field.Set(reflect.ValueOf(bson.NewObjectId()))
		fmt.Printf("NEW %+v\n", m)
		return c.Insert(m)
	} else {
		id := bson.ObjectId(id_field.String())
		fmt.Printf("UP %s %v\n", id, m)
		return c.UpdateId(id, m)
	}
	return nil
}

func C(id bson.ObjectId, m interface{}) error {
	collection := toCollectionName(m)
	c := DB.C(collection)


	id_field := reflect.ValueOf(m).Elem().FieldByName("Id")
	fmt.Printf("id_field %+v %+v\n", bson.ObjectId(id_field.String()), reflect.TypeOf(id_field))

	if id_field.String() == "" {
		id_field.Set(reflect.ValueOf(bson.NewObjectId()))
		fmt.Printf("NEW %+v\n", m)
		return c.Insert(m)
	} else {
		id := bson.ObjectId(id_field.String())
		fmt.Printf("UP %s %v\n", id, m)
		return c.UpdateId(id, m)
	}
	return nil
}


func ModelSetup(m Modeli) error {
	indexes := m.Indexes()
	for _, index := range indexes {
		DB.C(toCollectionName(m)).EnsureIndex(*index)
	}
	return nil
}

type User struct {
	Id bson.ObjectId `_id,omitempty`
	FirstName string 
	MiddleName string 
	LastName string
	Email string
	password string
}


func (m *User) Indexes() []*mgo.Index {
	email := mgo.Index {
		Key: []string{"email"},
		Unique: true,
		DropDups: true,
        Background: true, // See notes.
		Sparse: true,
	}

	return []*mgo.Index{ &email }
}

func (m *User) SetPassword(password string) {
	m.password = password + "a"
}

func (m *User) GetPassword() string {
	return m.password
}

type Subject struct {
	Id string `_id`
//	ParentId bson.ObjectId `parent_id,omitempty`
	Index int
	Title string
	Description string
	Subjects []Subject `bson: subjects`
}

func (s *Subject) Append(sc *Subject) {
	if s.Subjects == nil {
		s.Subjects = make([]Subject, 0, 5)
	}
	sc.Index = len(s.Subjects) + 1
	s.Subjects = append(s.Subjects, *sc)
	
	fmt.Printf("Appened %s to %s as %d\n", sc.Title, s.Title, sc.Index)
}

func (s *Subject) Print(parentIndex int) {
	fmt.Printf("%d.%d %s\n", parentIndex, s.Index, s.Title)
	if s.Subjects != nil {
		for _, sc := range(s.Subjects) {
			sc.Print(s.Index)
		}
	}
}
	

type Project struct {
	Id bson.ObjectId `_id,omitempty`
	Name string 
	Description string 
	Introduction string 
	Subjects []Subject `bson:"subjects"` 
}

func (p *Project) AppendSubject(s *Subject) {
	if p.Subjects == nil {
		p.Subjects = make([]Subject, 0, 5)
	}
	s.Index = len(p.Subjects) + 1
	p.Subjects = append(p.Subjects, *s)
	
}

func (p *Project) Print() {
	fmt.Printf("Project %s %s\n", p.Id, p.Name)
	fmt.Printf("Project %+v\n", p)
	if p.Subjects != nil {
		for _, s := range(p.Subjects) {
			s.Print(0)
		}
	}
}

func main() {
	DBC = ConnectMongo("localhost", "testdb")
	if DBC == nil {
		panic("Cannot connect")
	}
	//DB = DBC.Database
	defer DBC.Close()

	id := bson.NewObjectId()
	fmt.Printf("id %s %+v\n", bson.ObjectId(string(id)), id)
	os.Exit(0)

	p := &Project{Name: "multicookery", Description: "Gotowanie" }
	s := &Subject{Title: "Day #1", Description: "Ala ma kota"}
	s1 := &Subject{Title: "Question #1", Description: "Ala ma psa"}
	s2 := &Subject{Title: "Question #1", Description: "Ala ma psa"}

	s.Append(s1)
	s.Append(s2)
	p.AppendSubject(s)

	s = &Subject{Title: "Day #2", Description: "Ola ma kota"}
	s.Append(s1)
	s.Append(s2)
	p.AppendSubject(s)

    Save(p)
	p.Print()
	j, err := json.Marshal(*p)
	os.Stdout.Write(j)

	pp := Project{}
	fmt.Printf("\n\n");
	err = json.Unmarshal(j, &pp)
	fmt.Printf("%+v\n", pp)
	
	

	//fmt.Printf("S %s\n", p.Name)
	//fmt.Printf("L %+v\n", len(p.Subjects))

	
	os.Exit(0)

	c := DB.C("users")

	n, err := c.Count()
	fmt.Printf("C %d\n", n)

	iter := c.Find(nil).Iter()
	u := User{}



	for iter.Next(&u) {
		fmt.Printf("Result: %s %+v\n", string(u.Id), u)
		u.Email = fmt.Sprintf("dupablada@%s", u.Id)
		err = Save(&u)
		if err != nil {
			fmt.Printf("ERRUpdate %+v\n", err);
			os.Exit(1)
		}
		os.Exit(0)

	}

	if false {
		p := &User{FirstName: "Ala", MiddleName: "A.", LastName: "Kot", Email: "ala@makota111234.com"}
		err = Save(p)
		if err != nil {
			panic(err)
		}
		fmt.Printf("Saved %+v\n", p)
		err = Save(p)
		if err != nil {
			panic(err)
		}
	}

	//err = p.Save(db)
	//if err != nil {
//		panic(err)
//	}
//	fmt.Printf("ID %s\n", p.Id);
	/*
	result := Person{}
	
	err = c.Find(bson.M{"firstName": "Ale"}).One(&result)
	if err != nil {
		panic(err)
	}
	fmt.Println("Phone:", result.FirstName)
	fmt.Println("Phone:", result.Id)

	err = c.FindId(result.Id).One(&result)
	if err != nil {
		panic(err)
	}
	fmt.Println("Phone:", result.FirstName)
	fmt.Println("Phone:", result.Id)
*/
	
}
/*
func home(res http.ResponseWriter, req *http.Request) {
    fmt.Fprintf(res, "hello, world\n")
}


func main() {
    http.HandleFunc("/", homepage)
    fmt.Println("listening...")
    err := http.ListenAndServe(":"+os.Getenv("PORT"), nil)
    if err != nil {
      panic(err)
    }
}

*/