package movico

import (
	"io"
	"fmt"
	"html/template"
	"path/filepath"
	_ "github.com/eknkc/amber"
)

type AmberEngine int8

func (e AmberEngine) Name() string {
	return "amber"
}

func (e AmberEngine) Ext() string {
	return ".amber"
}

func (e AmberEngine) NewTemplate(layout, template string) (Templater, error) {
	return NewAmberTemplate(layout, template)
}


type AmberTemplate struct {
	*template.Template
	name     string
	path     string
	layout   string
	compiled bool
}


func NewAmberTemplate(layout, path string) (*AmberTemplate, error) {
	templateJar.Lock()
	defer templateJar.Unlock()

	name := path

	if t, ok := templateJar.Get(name); ok {
		tt := t.(*AmberTemplate)
		return tt, nil
	}

	t := &AmberTemplate{template.New(filepath.Base(layout)), name, path, layout, false}
	err := t.Compile()
	if err != nil {
		return nil, err
	}
	t.compiled = true
	//fmt.Printf("TemplateJar.cache = %+v %s %v\n", TemplateJar.cache, name, t)

	templateJar.Set(name, t)
	return t, nil
}

func (t *AmberTemplate) Name() string {
	return t.name
}

func (t *AmberTemplate) Compile() error {
	if t.compiled {
		return nil
	}
	fmt.Printf("Compile %s %s\n", t.path, t.layout)
	_, err := t.ParseFiles(t.layout, t.path)
	return err
}

func (t *AmberTemplate) Render(w io.Writer, data interface{}) error {
	return t.Execute(w, data)
}

/*
var funcs = template.FuncMap {
	//"reverse": reverse,
}

func Template(name, path, layout string) *template.Template {
	TemplateJar.mutex.Lock()
	defer TemplateJar.mutex.Unlock()

	if t, ok := TemplateJar.cache[name]; ok {
		return t
	}

	t := template.New(name).Funcs(funcs)
	t = template.Must(t.ParseFiles(
		"TemplateJar/_base.html",
		filepath.Join("TemplateJar", name),
	))
	TemplateJar.cache[name] = t

	return t
}
*/


