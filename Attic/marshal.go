package movico

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"mime"
)

type Marshaler interface {
	Name() string
	MimeType() string
	Marshal(v interface{}) ([]byte, error)
	MarshalError(error) ([]byte, error)
}

var marshalerRegistry = newRegistry()

func RegisterMarshaler(m Marshaler) {
	marshalerRegistry.set(m)
	mime.AddExtensionType("."+m.Name(), m.MimeType())
}

func getMarshaler(name string) (Marshaler, error) {
	e, ok := marshalerRegistry.get(name)
	if !ok {
		return nil, fmt.Errorf("%s: no such marshaler registered", name)
	}
	return e.(Marshaler), nil
}

type jsonMarshaler int8

func (m *jsonMarshaler) Name() string {
	return "json"
}

func (m *jsonMarshaler) MimeType() string {
	return "application/json"
}

func (m *jsonMarshaler) Marshal(v interface{}) ([]byte, error) {
	return json.Marshal(v)
}

type jsonError struct {
	Message string `json:"message"`
}

func (m *jsonMarshaler) MarshalError(err error) ([]byte, error) {
	return json.Marshal(jsonError{err.Error()})
}

type xmlMarshaler int8

func (m *xmlMarshaler) Name() string {
	return "xml"
}

func (m *xmlMarshaler) MimeType() string {
	return "application/xml"
}

func (m *xmlMarshaler) Marshal(v interface{}) ([]byte, error) {
	r, err := xml.Marshal(v)
	if err == nil {
		return r, nil
	}
	return nil, err
}

type xmlError struct {
	XMLName xml.Name `xml:"error"`
	Message string   `xml:"message"`
}

func (m *xmlMarshaler) MarshalError(err error) ([]byte, error) {
	return m.Marshal(xmlError{Message: err.Error()})
}

func init() {
	RegisterMarshaler(new(jsonMarshaler))
	RegisterMarshaler(new(xmlMarshaler))
}
