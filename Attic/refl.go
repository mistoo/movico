package movico

import (
	"reflect"
	"fmt"
)

//extract args from request
func decodeMethodArgs(method reflect.Value) (args []reflect.Value, ok bool) {
	methodType := method.Type()
	nargs := methodType.NumIn()

    args = make([]reflect.Value, nargs, nargs)

    for i := 0; i < nargs; i++ {
        in := methodType.In(i)
		
        fmt.Println("decodeMethodArgs index:", i, "type=", in, "kind=", in.Kind(), "name=", in.Name())
		
        switch in.Kind() {
        case reflect.Ptr:
			fmt.Printf("%d. ptr %s\n", i, in.Elem().Name())
            //if strings.Contains(in.Elem().Name(), "HttpContext") {
                //args[i] = reflect.ValueOf(ctx)
            //} else {
            //    Logger.Printf("incoming %v", in)
            //}
            //todo:
        case reflect.Struct:
            sv := reflect.New(in)
            sv = reflect.Indirect(sv)
            for i := 0; i < in.NumField(); i++ {
                f := in.Field(i)
                /*strv := ctx.Value(f.Name)
                    if strv == "" {
                        continue
                    }
				 */
                fValue := sv.FieldByName(f.Name)
                if !fValue.CanSet() {
                    continue
                }
				/*
                if v, err := ReflectValue(strv, f.Type); err == nil {
                    fValue.Set(v)
                } else {
                    fmt.Println("mvc filter decode struct err", in, f, err, strv)
                    return nil, false
                }*/
            }
            args[i] = sv
        }
    }

    return args, true
}
