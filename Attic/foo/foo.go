package foo

import "fmt"

type IController interface {
	nam() string
	Name() string
}

type Controller struct {
	n string
}

func (c *Controller) nam() string {
	return "controller"
}

func (c *Controller) Name() string {
	return "Controller"
}


func Hello(c IController) {
	fmt.Printf("c %s %s\n", c.nam(), c.Name())
}