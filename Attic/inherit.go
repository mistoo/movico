package main

import ( 
	"fmt"
	"reflect"
)

type ControllerInterface interface {
	Init(string)
}


type RouteConf struct {
	HandlerMethod string
	Path string
	Methods string
	Name string
	Formats string
}

type Controller struct {
	Name           string
	Layout         string
 	RoutesConf     map[string]RouteConf
}

func (c *Controller) Init(name string) {
	c.Name = name
/*
	c.RestRoutes = map[string]RouteConf {
		"Index"  : { Path: "/", Methods: "GET" },
		"Show"   : { Path: "/{id}", Methods: "GET" },
		"New"    : { Path: "/new", Methods: "GET" },
		"Create" : { Path: "/", Methods: "POST" },
		"Edit"   : { Path: "/edit", Methods: "GET" },
		"Update" : { Path: "/{id}", Methods: "POST" },
	}
*/
}

func (c *Controller) RouteConfFor(method string) *RouteConf {
	return nil
}


func (c *Controller) Render() string {
	fmt.Printf("Rendering %s\n", c.Name)
	return c.Name
}

type UsersController struct {
	Controller
	
}

/*func (uc *UsersController) Init() {
	uc.Controller.Init()
}*/

func (uc *UsersController) Show() {
	fmt.Printf("Showing %s\n", uc.Name)
}

func initController(ci ControllerInterface) {
	t := reflect.Indirect(reflect.ValueOf(ci)).Type()
	fmt.Printf("%+v type is %+v\n", ci, t)

	m := reflect.ValueOf(ci).MethodByName("Init")
		
	in := make([]reflect.Value, 1)
	in[0] = reflect.ValueOf(t.Name())

	m.Call(in)
	fmt.Printf("%+v type is %+v\n", ci, t)

	//c := reflect.New(t)
	//fmt.Printf("%+v\n", c)
	//m := c.MethodByName("Init")

	//in := make([]reflect.Value, 1)
	//ct := &Context{ResponseWriter: w, Request: r, Params: params}
	//in[0] = reflect.ValueOf(ct)
	//init.Call(in)

	//t.ControllerInterface
}


func main() {
	x := (*UsersController)(nil)
	fmt.Printf("%+v %+v\n", x, reflect.TypeOf(x))
	v1 := reflect.TypeOf(x).Elem()
	fmt.Printf("%+v\n", v1)

	//uc := UsersController{}
	initController(&UsersController{})
}
