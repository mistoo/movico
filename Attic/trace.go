package logplus

import (
	"bytes"
	"fmt"
	"runtime"
)

func StackN(n int) string {
	b := bytes.NewBufferString("")
	for x := 1; x < n; x++ {
		pc, file, line, ok := runtime.Caller(x)
		if ok {
			b.WriteString(
				fmt.Sprintf("Function: %s\nFile: %s\nLine: %d\n\n",	runtime.FuncForPC(pc).Name(), file, line,),)
		}
	}
	return b.String()
}