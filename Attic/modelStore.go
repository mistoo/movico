type modelStore struct {
	models map[string]Modeler
	modelers map[Modeler]string
}

func (store *modelStore) ClassName(m Modeler) string {
	if name, ok := store.modelers[m]; ok {
		return name
	}
	panic(errors.New("Model %v not found"))
}

func (store *modelStore) AddModel(m Modeler) {
	v := reflect.ValueOf(m)
	if v.Kind() != reflect.Ptr || v.Elem().Kind() != reflect.Struct {
		panic(errors.New("AddModel: Modeler must be a pointer to struct"))
	}
		
	className := reflect.Indirect(v).Type().Name()

	store.models[className] = m
	store.modelers[m] = className
}

