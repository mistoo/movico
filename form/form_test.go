package form

import (
	"testing"
	"net/url"
	"github.com/orfjackal/gospec/src/gospec"
	//. "github.com/orfjackal/gospec/src/gospec"
	"fmt"
)


func FormSpec(c gospec.Context) {
	c.Specify("Unbounded form", func() {
		f := NewForm("auth")
		f.AddTextField("email")
		f.AddPasswordField("password")
		
		v := url.Values{
			"email": []string{"aa@foo.com"},
		}

		fmt.Println(f.Instance().Action("/auth").SetData(v).Render())
	})

	c.Specify("Bounded form", func() {
		
		f := NewFormFor("user")
		f.AddTextField("email")
		f.AddPasswordField("password")
		
		v := url.Values{
			"email": []string{"aa@foo.com"},
		}

		fmt.Println(f.Instance().Action("/auth").Method("PUT").SetData(v).Render())
	})

}


func TestAllSpecs(t *testing.T) {
	r := gospec.NewRunner()

	// List all specs here
	r.AddSpec(FormSpec)
	// Run GoSpec and report any errors to gotest's `testing.T` instance
	gospec.MainGoTest(r, t)
}
