package form

import (
	"fmt"
	"strings"
	"net/http"
	"net/url"
	"html/template"

	"bitbucket.org/pkg/inflect"
	h "github.com/metakeule/goh4"
)

type Form struct {
	Name string
	For string
	Fields []FieldInterface
	Buttons []FieldInterface
	FieldMap map[string]FieldInterface
	Validators []func(*http.Request) (bool)
	attributes map[string]string
	readonly bool
}


type FieldInterface interface {
	Id() string
	Name() string
	PrefixedName() string
	Label() string
	SetLabel(string) FieldInterface
	Type() string
	Render(values []string) *h.Element
	RenderErrors() string
	RenderLabel() *h.Element
	Validate(*http.Request) bool
	SetModelName(string)
	SetAttr(name, value string) 
}

type Field struct {
	fieldType string 
	id string
	name string
	prefixedName string
	prefixedId string
	label string
	attributes map[string]string
	Errors []string
	Validators []func (*Field, *http.Request) (bool)
}

func newField(name, fieldType string) Field {
	field := Field{fieldType: fieldType, name: name, prefixedId: name, prefixedName: name}
	field.label = inflect.Humanize(name)
	field.id = fmt.Sprintf("%s", field.name)
	field.prefixedName = field.name
	field.prefixedId = field.id
	field.attributes = make(map[string]string)
	return field
}

func (field *Field) SetModelName(name string) {
	field.prefixedName = fmt.Sprintf("%s[%s]", name, field.name)
	field.prefixedId = fmt.Sprintf("%s_%s", name, field.name)
}

func (field *Field) SetAttr(name, value string) {
	field.attributes[name] = value
}

func (field *Field) Attr(name string) string {
	return field.attributes[name]
}

func (field *Field) Type() string { return field.fieldType }
func (field *Field) Id() string { return field.prefixedId }
func (field *Field) Name() string { return field.name }
func (field *Field) PrefixedName() string { return field.prefixedName }
func (field *Field) Label() string { return field.label }

func (field *Field) SetLabel(label string) FieldInterface {
	field.label = label
	return field
}

func (field *Field) Render(values []string) *h.Element {
	return h.Span(h.Text("not implemented"))
}

func (field *Field) renderInput(values []string) *h.Element {
	e := h.Input(h.Attr("type", field.fieldType, "name", field.prefixedName), 
		h.Id(field.prefixedId), h.Class(field.fieldType))
	if len(values) > 0 {
		e.SetAttribute("value", values[0])
	}
	
	return e
}

func (field *Field) RenderErrors() (string) {
	out := ""
	if len(field.Errors) != 0 {
		out += "<ul class=\"error_list\">"
		for _, errstr := range field.Errors {
			out += fmt.Sprintf(`<li>%s</li>`, errstr)
		}
		out += "</ul>"
	}
	return out
}

func (field *Field) RenderLabel() *h.Element {
	return h.Label(h.Attr("for", field.prefixedId), h.Id(field.prefixedId + "_label"), 
		h.Text(field.label))
}

func (field *Field) Validate(r *http.Request) (bool) {
	valid := true
	for _, validator := range field.Validators {
		valid = validator(field, r) && valid
	}
	return valid
}

type TextField struct { Field }
func newTextField(name string) *TextField {
	f := newField(name, "text")
	return &TextField{Field: f}
}

func (field *TextField) Render(values []string) *h.Element {
	return field.Field.renderInput(values)
}


type PasswordField struct { Field }
func newPasswordField(name string) *PasswordField {
	f := newField(name, "password")
	return &PasswordField{Field: f}
}

func (field *PasswordField) Render(values []string) *h.Element {
	return field.Field.renderInput(values)
}

type HiddenField struct { Field }
func newHiddenField(name string) *HiddenField {
	f := newField(name, "hidden")
	return &HiddenField{Field: f}
}

func (field *HiddenField) Render(values []string) *h.Element {
	return field.Field.renderInput(values)
}

func (field *HiddenField) RenderLabel() *h.Element {
	return nil
}

type TextArea struct { Field }
func newTextArea(name string) *TextArea {
	f := newField(name, "textarea")
	return &TextArea{Field: f}
}

func (field *TextArea) Render(values []string) *h.Element {
	if len(values) > 0 {
		return h.Textarea(h.Attr("name", field.prefixedName), h.Id(field.prefixedId), h.Text(values[0]))
	}
	
	return h.Textarea(h.Attr("name", field.prefixedName), h.Id(field.prefixedId))
}

type Option struct {
	value string
	label string
}

type Select struct {
	Field
	options []Option
}

func newSelect(name string, options []Option) *Select {
	f := newField(name, "select")
	return &Select{Field: f, options: options}
}

func (field *Select) Render(values []string) *h.Element {
	e := h.Select(h.Attr("name", field.prefixedName), h.Id(field.prefixedId))

	for _, o := range field.options {
		oe := h.Option(h.Attr("value", o.value), h.Text(o.label))
		for _, v := range values {
			if v == o.value {
				oe.SetAttribute("selected", "selected")
			}
		}

		e.Add(oe)
	}
	return e
}

type RadioGroup struct {
	Field
	Options []Option
}

func newRadio(name string, options []Option) *RadioGroup {
	f := newField(name, "radio")
	return &RadioGroup{Field: f, Options: options}
}

func (field *Field) renderGroup(options []Option, values []string) *h.Element {
	e := h.Span(h.Id(field.prefixedId))
	for _, o := range options {
		oid := field.prefixedId + "_" + o.value
		elem := h.Input(h.Attr("type", field.fieldType, "name", field.prefixedName, "value", o.value), h.Id(oid))
		for _, v := range values {
			if v == o.value {
				elem.SetAttribute("checked", "checked")
			}
		}
		label := h.Label(h.Attr("for", oid), h.Id("label_" + oid), h.Text(o.label))
		
		e.Add(h.Span(elem, label))
	}
	return e
}

func (field *RadioGroup) Render(values []string) *h.Element {
	return field.renderGroup(field.Options, values)
}

type CheckboxGroup struct {
	Field
	Options []Option
}

func newCheckboxGroup(name string, options []Option) FieldInterface {
	f := newField(name, "checkbox")
	return &CheckboxGroup{Field: f, Options: options}
}

func (field *CheckboxGroup) Render(values []string) *h.Element {
	return field.renderGroup(field.Options, values)
}


type Checkbox struct { Field }
func newCheckbox(name string) FieldInterface {
	f := newField(name, "checkbox")
	return &Checkbox{Field: f}
}

func (field *Checkbox) Render(values []string) *h.Element {
	return field.renderInput(values)
}

type Button struct { 
	Field
	buttonType string
}

func newButton(name string, buttonType string) FieldInterface {
	f := newField(name, "button")
	return &Button{Field: f, buttonType: buttonType}
}


func (field *Button) Render(values []string) *h.Element {
	e := h.Button(h.Attr("name", field.prefixedName, "type", field.buttonType), h.Id(field.prefixedId), h.Text(field.label))
	return e
}

func (field *Button) RenderLabel() *h.Element {
	return nil
}

type File struct { Field }

func newFile(name string) FieldInterface {
	f := newField(name, "file")
	return &File{Field: f}
}

func (field *File) Render(values []string) *h.Element {
	return field.Field.renderInput(values)
}

/* Form */

func NewForm(name string) *Form {
	f := new(Form)
	//f.Fields []FieldInterface
	f.FieldMap = make(map[string]FieldInterface)
	f.Name = name
	f.attributes = make(map[string]string)
	f.attributes["enctype"] = "application/x-www-form-urlencoded"
	f.attributes["method"] = "post"
	f.readonly = false
	return f
}

func NewFormFor(name string) *Form {
	f := NewForm(name)
	f.For = strings.ToLower(name)
	return f
}

func (f *Form) SetAttribute(name string, value string) *Form {
	f.attributes[name] = value
	return f
}

func (form *Form) AddField(field FieldInterface) {
	if form.For != "" {
		field.SetModelName(form.For)
	}
	
	form.Fields = append(form.Fields, field)
	form.FieldMap[field.Name()] = field
	
	if _, ok := field.(*File); ok {
		form.attributes["enctype"] = "multipart/form-data"
		form.attributes["method"] = "post"
	}
}

func (form *Form) AddTextField(name string) { form.AddField(newTextField(name)) }
func (form *Form) AddPasswordField(name string) { form.AddField(newPasswordField(name)) }
func (form *Form) AddHiddenField(name string) { form.AddField(newHiddenField(name)) }
func (form *Form) AddTextArea(name string) { form.AddField(newTextArea(name)) }
func (form *Form) AddSelect(name string, options []Option) { form.AddField(newSelect(name, options)) }
func (form *Form) AddFileField(name string) { form.AddField(newFile(name)) }

func (form *Form) AddButton(field FieldInterface) {
	form.Buttons = append(form.Buttons, newButton("Save", "submit"))
}

func (form *Form) AddSubmit(label string) {
	form.Buttons = append(form.Buttons, newButton(label, "submit"))
}


type FormInstance struct {
	form *Form
	attributes map[string]string
	data url.Values
}


func (form *Form) Instance() *FormInstance {
	fi := new(FormInstance)
	fi.form = form
	fi.attributes = make(map[string]string)
	fi.attributes["method"] = form.attributes["method"]
	return fi
}

func (fi *FormInstance) Action(url string) *FormInstance { 
	fi.attributes["action"] = url
	return fi
}

func (fi *FormInstance) Method(method string) *FormInstance { 
	fi.attributes["method"] = strings.ToLower(method)
	return fi
}

func (f *FormInstance) SetData(values url.Values) *FormInstance { 
	f.data = values
	return f
}
	
func (fi *FormInstance) formElem() *h.Element {
	e := h.Form(h.Class("form"), h.Id(fi.form.Name + "_form"), h.Attr("name", fi.form.Name))


	action := fi.attributes["action"]
	method, ok := fi.attributes["method"]
	if !ok {
		method = "post"
	}

	if method == "put" || method == "delete" {
		action += "?_method=" + method
		method = "post"
	}
	e.SetAttribute("method", method)
	e.SetAttribute("action", action)
	return e
}



func (fi *FormInstance) Render() *h.Element {
	t := h.Table(h.Class("form"), h.Id(fi.form.Name + "_form"))

	emptyValues := make([]string, 0)
	for _, field := range fi.form.Fields {
		if _, ok := field.(*Button); ok {
			continue
		}
		fmt.Printf("Rendering %s %s\n", field.Name(), field.Id())
		th := h.Th(h.Class("control-label"), h.Html(field.RenderLabel().String()))

		fvalues := fi.data[field.Name()]
		if fvalues == nil {
			fvalues = emptyValues
		}

		html := field.Render(fvalues).String()
		td := h.Td(h.Class("control-field"), h.Html(html))
		
		row := h.Tr(th, td, h.Class("control"), h.Id(field.Id() + "_control"))
		t.Add(row)
	}

	row := h.Tr(h.Class("buttons"))
	td := h.Td(h.Class("buttons"), h.Attr("colspan", "2", "nowrap", "nowrap"))
	for _, b := range fi.form.Buttons {
		td.Add(h.Html(b.Render(nil).String()))
	}
	row.Add(td)
	t.Add(row)

	e := fi.formElem()
	for k, v := range fi.form.attributes {
		if k == "method" || k == "action" {
			continue
		}
		e.SetAttribute(k, v)
	}
	e.Add(t)
	return e
}

func (fi *FormInstance) RenderHTML() template.HTML {
	return template.HTML(fi.Render().String())
}


func (form *Form) Validate(r *http.Request) (bool) {
	valid := true
	for _, field := range form.Fields {
		valid = field.Validate(r) && valid
	}
	return valid
}

/*
func main() {
	form := NewFormFor("person")
	form.AddTextField("email")
	form.AddTextArea("body")
	f := form.Clone()
	f.SetData(map[string]string { "email" : "foo@bar.com" })
	fmt.Println(f.Render().String())
	fmt.Println(f.Data())
}
*/
