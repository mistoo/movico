package form

import (
	"fmt"
	"regexp"
	"net/http"
	//"bitbucket.org/pkg/inflect"
	//h "github.com/metakeule/goh4"
	//"html/template"
)

/* Validators */

type Validator func (field *Field, r *http.Request) (bool)


func (field *Field) ValidateRequired() {
	field.Validators = append(field.Validators, func (field *Field, r *http.Request) (bool) {
		name := field.name
		value, ok := r.Form[name]
		ret := false
		if ok {
			if 0 != len(value) && 0 != len(value[0]) {
				ret = true
			}
		}
		if !ret {
			field.Errors = append(field.Errors, fmt.Sprintf("%s field is required", field.Label))
		}
		return ret
	})
}

func (field *Field) ValidateMatchRegexp(re *regexp.Regexp) {
	field.Validators = append(field.Validators, func (field *Field, r *http.Request) (bool) {
		name := field.name
		value, ok := r.Form[name]
		ret := false
		if ok {
			if 0 != len(value) && re.Match([]byte(value[0])) {
				ret = true
			}
		}
		if !ret {
			field.Errors = append(field.Errors, fmt.Sprintf("%s is not valid", field.Label))
		}
		return ret
	})
}
