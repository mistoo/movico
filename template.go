package movico

import (
	"fmt"
	"html/template"
	"io"
	"sync"
)

type TemplateEngine interface {
	Name() string
	Ext() string
	NewTemplate(layout, template string, funcMap template.FuncMap) (Templater, error)
}

type Templater interface {
	Name() string
	Compile() error
	Funcs(funcMap template.FuncMap)
	Render(w io.Writer, data interface{}) error
}

var templateEngines = newRegistry("templateEngines")

func RegisterTemplateEngine(engine TemplateEngine) {
	templateEngines.set(engine)
}

func getTemplateEngine(name string) (TemplateEngine, error) {
	e, ok := templateEngines.get(name)
	if !ok {
		return nil, fmt.Errorf("%s: no such template engine registered", name)
	}
	return e.(TemplateEngine), nil
}

type templateCache struct {
	_cache map[string]Templater
	_mutex sync.Mutex
}

var templateJar *templateCache

func (c *templateCache) Lock() {
	c._mutex.Lock()
}

func (c *templateCache) Unlock() {
	c._mutex.Unlock()
}

func (c *templateCache) Get(name string) (t Templater, ok bool) {
	t, ok = c._cache[name]
	return
}

func (c *templateCache) Set(name string, t Templater) {
	c._cache[name] = t
}

func init() {
	templateJar = &templateCache{_cache: make(map[string]Templater)}
	//fmt.Printf("TemplateJar.cache = %+v\n", TemplateJar)
}
