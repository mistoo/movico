package movico

import (
	"fmt"
	"mime"
	"net/http"
)

type Renderer interface {
	SetLayout(layoutPath string)
	SetTemplate(templatePath string)
	Render(w http.ResponseWriter, pairs ...interface{}) error
	RenderError(w http.ResponseWriter, re *Response) error
}

type RendererModule interface {
	MimeType(name string) string
	NewRenderer(app *Application, req *Request) (Renderer, error) 
}
	
var rendererRegistry = newRegistry("renderer")

func RegisterRendererModule(name string, m RendererModule) {
	rendererRegistry.setU(name, m)
	mime.AddExtensionType("."+ name, m.MimeType(name))
}

func getRendererModule(name string) (RendererModule, error) {
	e, ok := rendererRegistry.get(name)
	if !ok {
		return nil, fmt.Errorf("%s: no such renderer module registered", name)
	}
	return e.(RendererModule), nil
}

func (app *Application) newRenderer(req *Request) (Renderer, error) {
	r, err := getRendererModule(req.Format)
	if err != nil && req.Format != "html" { // fallback to HTML
		r, err = getRendererModule("html")
	}
	if err != nil { return nil, err }

	return r.NewRenderer(app, req)
}

