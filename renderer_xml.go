package movico
// XML renderer

import (
	"net/http"
	"encoding/xml"
)

type xmlMarshaler int8
	
func (m *xmlMarshaler) MimeType(name string) string {
	return "application/xml"
}

func (m *xmlMarshaler) SetLayout(layoutPath string) {}
func (m *xmlMarshaler) SetTemplate(templatePath string) {}

func (m *xmlMarshaler) NewRenderer(app *Application, req *Request) (Renderer, error) {
	return m, nil // return itself as xmlMarshaler implements both RendererModule and Renderer interfaces
}

func (m *xmlMarshaler) Render(w http.ResponseWriter, v ...interface{}) error {
	b, err := xml.Marshal(v[1])
	if err != nil {
		return err
	}
	_, err = w.Write(b)
	return err
}

type xmlError struct {
	XMLName xml.Name `xml:"error"`
	Message string   `xml:"message"`
}

func (m *xmlMarshaler) RenderError(w http.ResponseWriter, re *Response) error {
	b, err := xml.Marshal(xmlError{Message: re.String()})
	if err == nil {
		_, err = w.Write(b)
	}
	return err
}

func init() {
	RegisterRendererModule("xml", new(xmlMarshaler))
}
