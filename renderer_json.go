package movico
// Movico's JSON renderer

import (
	"encoding/json"
	"net/http"
)

type jsonMarshaler int8
	
func (m *jsonMarshaler) MimeType(name string) string {
	return "application/json"
}

func (m *jsonMarshaler) SetLayout(layoutPath string) {}
func (m *jsonMarshaler)	SetTemplate(templatePath string) {}


func (m *jsonMarshaler) NewRenderer(app *Application, req *Request) (Renderer, error) {
	return m, nil // return itself as jsonMarshaler implements both RendererModule and Renderer interfaces
}

func (m *jsonMarshaler) Render(w http.ResponseWriter, v ...interface{}) error {
	b, err := json.Marshal(v[1])
	if err != nil {
		return err
	}
	_, err = w.Write(b)
	return err
}

type jsonError struct {
	Message string `json:"message"`
}

func (m *jsonMarshaler) RenderError(w http.ResponseWriter, re *Response) error {
	b, err := json.Marshal(jsonError{re.String()})
	if err == nil {
		_, err = w.Write(b)
	}
	return err
}

func init() {
	RegisterRendererModule("json", new(jsonMarshaler))
}
