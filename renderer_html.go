package movico

import (
	"fmt"
	"errors"
	"net/http"
	"path/filepath"
	"html/template"
	"path"
	"strings"
	"sync"
)

type htmlModule struct {
	errorTemplates map[int]string
	once           sync.Once
}
	
func (m *htmlModule) MimeType(name string) string {
	if name == "html" {
		return "text/html"
	}

	if name == "text" {
		return "text/plain"
	}

	return ""
}

type htmlView struct {
	mod  *htmlModule
	app  *Application
	req  *Request

	templatePath string
	layoutPath   string

	template  Templater
	uriPrefix string
	assetsJS  []string
	assetsCSS []string
}

func (m *htmlModule) NewRenderer(app *Application, req *Request) (Renderer, error) {
	v := &htmlView{mod: m, app: app, req: req}
	v.uriPrefix = app.StaticPrefix
	return v, nil
}

func (v *htmlView) SetLayout(layoutPath string) {
	v.layoutPath = layoutPath
}

func (v *htmlView) SetTemplate(templatePath string) {
	v.templatePath = templatePath
}

func (v *htmlView) prepare() error {
	if v.template != nil {
		return fmt.Errorf("Template cannot prepared twice")
	}
	m := v.dummyFuncMap(template.FuncMap{})
	t, err := v.app.TemplateEngine.NewTemplate(v.layoutPath, v.templatePath, m)
	if err != nil {
		return fmt.Errorf("Template initialization error: %s", err)
	}
	t.Funcs(v.funcMap(m))
	v.template = t
	return nil
}

func (v *htmlView) Render(w http.ResponseWriter, data ...interface{}) error {
	if len(data) % 2 != 0 {
		return fmt.Errorf("Odd number of data arguments (%d) to htmlView.render()", len(data))
	}

	args, err := sliceToMap(data)
	if err != nil {
		return err
	}

	args["Request"] = v.req
	args["Controller"] = v.req.ControllerName
	args["Method"] = v.req.MethodName
	args["Session"] = v.req.Session.Values
	args["Params"] = v.req.Params()
	args["App"] = v.app
	
	if v.template == nil {
		v.prepare()
	}

	if err := v.template.Render(w, args); err != nil {
		return err
	}
	return nil
}

func (v *htmlView) setupErrTemplates() {
	if v.mod.errorTemplates != nil {
		return
	}

	v.mod.errorTemplates = make(map[int]string)
	files, err := filepath.Glob(path.Join(v.app.ViewsPath, "errors", "*.html"))

	if err != nil {
		return
	}

	tmap := map[string]int{
		"not_found":             http.StatusNotFound,
		"forbidden":             http.StatusForbidden,
		"default":               500,
	}

	for i := range files {
		name := strings.Replace(filepath.Base(files[i]), ".html", "", -1)
		status, ok := tmap[name]
		if !ok {
			v.app.Log.Error("%s: unknown error template, skipped", files[i])
			continue
		}
		v.mod.errorTemplates[status] = files[i]
	}
}

func (v *htmlView) renderErrorTemplate(w http.ResponseWriter, rerr error, templatePath string) error {
	v.SetTemplate(templatePath)

	err := v.Render(w, "error", rerr, "backtrace", v.app.Backtrace("html"))
	if err != nil {
		return fmt.Errorf("%s: render error: %s", templatePath, err)
	}
	return nil
}

func (v *htmlView) RenderError(w http.ResponseWriter, re *Response) error {
	v.mod.once.Do(func() {
		v.setupErrTemplates()
	});

	if tpath, ok := v.mod.errorTemplates[re.Code()]; ok {
		err := v.renderErrorTemplate(w, errors.New(re.String()), tpath)
		if err != nil {
			return err
		}
	}
	return nil  
}

func templateDummy0() template.HTML {
	return ""
}

func templateDummy1s(name string) template.HTML {
	return ""
}

func (v *htmlView) dummyFuncMap(m template.FuncMap) template.FuncMap {
	m["model_url"] = func(m Modeler) string { return "" }

	m["image_tag"] = templateDummy1s
	m["icon_tag"] = templateDummy1s

	m["css_link_tag"] = templateDummy1s
	m["js_link_tag"] = templateDummy1s

	m["require_js"] = templateDummy1s
	m["include_required_js"] = templateDummy0

	m["require_css"] = templateDummy1s
	m["include_required_css"] = templateDummy0

	m["app_prefix"] = templateDummy0
	m["format_error"] = templateDummy1s

	return m
}

func formatError(err error) template.HTML {
	s := strings.Split(err.Error(), ":")
	r := `<ul class="error">`
	for i := range s {
		r += fmt.Sprintf(`<li class="indent%d">%s</li>`, i, s[i])
	}
	return template.HTML(r + "</ul>")
}

func (v *htmlView) funcMap(m template.FuncMap) template.FuncMap {
	if m == nil {
		m = template.FuncMap{}
	}

	m["model_url"] = func(model Modeler) string {
		return v.app.ModelUrl(model)
	}

	m["method_url"] = func(toController, toMethod string, pairs ...string) string {
		return v.app.Url(toController, toMethod, pairs...)
	}

	m["url"] = func(toController, toMethod string, pairs ...string) string {
		return v.app.Url(toController, toMethod, pairs...)
	}

	m["absolute_url"] = func(toController, toMethod string, pairs ...string) string {
		u, err := v.app.URL(toController, toMethod, pairs...)
		if err != nil {
			panic(err)
		}
		return v.req.AbsolutizeURL(u).String()
	}

	m["image_tag"] = v.image_tag
	m["icon_tag"] = v.icon_tag

	m["css_link_tag"] = v.css_link_tag
	m["js_link_tag"] = v.js_link_tag
	m["require_js"] = v.require_js
	m["include_required_js"] = v.include_required_js
	m["require_css"] = v.require_css
	m["include_required_css"] = v.include_required_css

	m["app_prefix"] = v.app_prefix
	m["format_error"] = formatError

	return m
}

func (v *htmlView) app_prefix() string {
	return v.app.RoutePrefix
}

func (v *htmlView) image_tag(name string) template.HTML {
	ext := ""
	if !strings.Contains(name, ".") {
		ext = ".png"
	}

	return template.HTML(fmt.Sprintf(`<img src="%s/img/%s%s" />`, v.uriPrefix, name, ext))
}

func (v *htmlView) icon_tag(name string) template.HTML {
	return v.image_tag("icons/" + name)
}

func (v *htmlView) require_js(name string) string {
	v.assetsJS = append(v.assetsJS, name)
	//fmt.Printf("** requireJS %s, %d\n", name, len(v.assetsJS))
	return ""
}

func (v *htmlView) require_css(name string) string {
	v.assetsCSS = append(v.assetsCSS, name)
	//fmt.Printf("** requireCSS %s\n", name)
	return ""
}

func (v *htmlView) asset_uri(file string) string {
	return path.Join(v.uriPrefix, file)
}

func (v *htmlView) css_link_tag(name string) template.HTML {
	u := path.Join(v.uriPrefix, "css")
	t := fmt.Sprintf(`<link href="%s/%s.css" media="all" rel="stylesheet" type="text/css" />`, u, name)
	return template.HTML(t)
}

func (v *htmlView) js_link_tag(name string) template.HTML {
	u := path.Join(v.uriPrefix, "js")
	t := fmt.Sprintf(`<script type="text/javascript" src="%s/%s.js"></script>`, u, name)
	return template.HTML(t)
}

func (v *htmlView) include_required_js() (out template.HTML) {
	//fmt.Printf("** N requiredJS %d\n", len(v.assetsJS))
	for _, file := range v.assetsJS {
		//fmt.Printf("** INCLUDE requiredJS %s\n", file)
		u := path.Join(v.uriPrefix, "js")
		t := fmt.Sprintf(`<script type="text/javascript" src="%s/%s.js"></script>`, u, file)
		out += template.HTML(t)
	}
	return out
}

func (v *htmlView) include_required_css() (out template.HTML) {
	//fmt.Printf("** N requiredCSS %d\n", len(v.assetsJS))
	for _, file := range v.assetsCSS {
		//fmt.Printf("** INCLUDE requiredCSS %s\n", file)
		u := path.Join(v.uriPrefix, "css")
		t := fmt.Sprintf(`<link href="%s/%s.css" media="all" rel="stylesheet" type="text/css" />`, u, file)
		out += template.HTML(t)
	}
	return out
}

func sliceToMap(pairs []interface{}) (map[string]interface{}, error) {
	m := make(map[string]interface{})

	if len(pairs)%2 != 0 {
		return nil, fmt.Errorf("Odd number of arguments (%d)", len(pairs))
	}

	for i := 0; i < len(pairs); i = i + 2 {
		key, ok := pairs[i].(string)
		if !ok {
			return nil, fmt.Errorf("%d argument is not a string (%v)", i, pairs[i])
		}
		m[key] = pairs[i+1]
	}

	return m, nil
}

func init() {
	RegisterRendererModule("html", new(htmlModule))
}

