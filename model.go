package movico

import (
	"fmt"
	"reflect"
	"strings"
)

type Modeler interface {
	ObjectId() string
}

func ModelerName(model Modeler) (string, error) {
	v := reflect.ValueOf(model)
	if v.Kind() != reflect.Ptr || v.Elem().Kind() != reflect.Struct {
		return "", fmt.Errorf("ModelerName(%+v): argument must be a pointer to struct", model)
	}
	return strings.ToLower(reflect.Indirect(v).Type().Name()), nil
}
