package movico

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"

	"github.com/gorilla/mux"
)

type Route struct {
	muxRoute  *mux.Route
	methodFQN string

	name    string
	path    string
	methods []string

	host    string
	schemes []string
	queries []string

	handler func(http.ResponseWriter, *http.Request)

	formats   []string
	formatmap map[string]bool
	pathArgs  map[string]bool
}

func newRoute(controller string, action string, path string) *Route {
	r := &Route{path: path}
	r.methodFQN = fmt.Sprintf("%s.%s", controller, action)
	r.Method("GET")

	if strings.Contains(path, "{") {
		args := extractPathVars(path)
		if len(args) > 0 {
			pathArgs := make(map[string]bool)
			for _, name := range args {
				pathArgs[name] = true
			}
			r.pathArgs = pathArgs
		}
	}
	r.Formats("html")
	return r
}

func (r *Route) URL(a ...string) (*url.URL, error) {
	return r.muxRoute.URL(a...)
}

func (r *Route) GetHandler() http.Handler {
	return r.muxRoute.GetHandler()
}

func (r *Route) Name(name string) *Route {
	//fmt.Printf("RRRR %s %s\n", r.path, name)
	r.name = name
	return r
}

func (r *Route) Method(m string) *Route {
	return r.Methods(m)
}

func (r *Route) Methods(m ...string) *Route {
	for k, v := range m {
		m[k] = strings.ToUpper(v)
	}
	r.methods = m
	return r
}

func (r *Route) Host(m string) *Route {
	r.host = m
	return r
}

func (r *Route) Schemes(m ...string) *Route {
	r.schemes = m
	return r
}

func (r *Route) Queries(m ...string) *Route {
	r.queries = m
	return r
}

func (r *Route) Formats(m ...string) *Route {
	r.formats = m
	r.formatmap = make(map[string]bool)

	for _, v := range r.formats {
		r.formatmap[v] = true
	}
	return r
}

func (r *Route) handlerFunc(handler func(http.ResponseWriter, *http.Request)) {
	r.handler = handler
}

func (r *Route) apply(route *mux.Route) (*mux.Route, error) {
	//route := router.Path(r.path).HandlerFunc(r.handler)

	if r.methods != nil {
		route.Methods(r.methods...)
	}

	if r.name != "" {
		route.Name(r.name)
	}

	if r.schemes != nil {
		route.Schemes(r.schemes...)
	}

	if r.host != "" {
		route.Host(r.host)
	}

	if r.queries != nil {
		route.Queries(r.queries...)
	}

	if r.handler == nil {
		panic(fmt.Errorf("%v %s - no handler defined", r.methods, r.path))
	}
	route.HandlerFunc(r.handler)

	return route, route.GetError()
}

func (r *Route) respondsTo(format string) bool {
	_, ok := r.formatmap[format]
	return ok
}
