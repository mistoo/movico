package movico

import (
	"fmt"
)

type DatabaseSession interface {
	Close()
}

type Database interface {
	Name() string
	Connect(config map[string]string) error
	Disconnect()
	OpenSession() DatabaseSession
}

var databases = newRegistry("db")

func RegisterDatabase(db Database) {
	databases.set(db)
}

func getDatabase(name string) (Database, error) {
	de, ok := databases.get(name)
	if !ok {
		return nil, fmt.Errorf("%s: no such database engine registered", name)
	}
	return de.(Database), nil
}
