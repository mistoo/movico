package movico

import (
	"fmt"
	"github.com/orfjackal/gospec/src/gospec"
	. "github.com/orfjackal/gospec/src/gospec"
	"os"
	"strings"
	//"bytes"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
)

func AppSpec(c gospec.Context) {
	testConfig := map[string]string{
		"env":            "production",
		"name":           "application",
		"title":          "Application Title",
		"version":        "111",
		"listen":         "127.0.0.1",
		"port":           "9999",
		"templates":      "native",
		"root":           "",
		"prefix":         "/app",
		"sessionsStore":  "cookie",
		"sessionsId":     "movicoSession",
		"sessionsSecret": "secret",
		"sessionsMaxAge": "0",
	}
	app := initTestApp(testConfig)

	c.Specify("Application", func() {
		c.Assume(app.RoutePrefix, Equals, "/app")

		c.Specify("Should read config", func() {
			for k, v := range testConfig {
				if !strings.HasPrefix(k, "sessions") {
					c.Expect(app.config[k], Equals, v)
				}
			}
			c.Expect(app.config["prefix"], Equals, app.RoutePrefix)

			c.Specify("Root directory should be taken from config", func() {
				c.Expect(app.config["root"], Equals, app.RootPath)
				wd, _ := os.Getwd()
				c.Expect(app.RootPath, Equals, wd)
			})

			c.Specify("Custom config values should be ignored", func() {
				c.Expect(app.config["foo"], Equals, "")
			})
		})

		c.Specify("Should handle development config transparently", func() {
			testConfig["env"] = "development"
			app := initTestApp(testConfig)

			c.Expect(app.config["version"], Equals, testConfig["version"]+"dev")
			c.Expect(app.config["sessions.id"], Equals, testConfig["sessionsId"]+"dev")
		})

		c.Specify("Should handle REST routes", func() {
			app.AddController(&People{})
			c.Assume(len(app.routes), Equals, 3)

			prefix := app.RoutePrefix

			c.Expect(app.Url("People", "Show", "id", "123"), Equals, prefix+"/people/123")
			c.Expect(app.Url("People", "Show", "id", "alamakota"), Equals, prefix+"/people/alamakota")
			c.Expect(app.Url("People", "Show", "id", "123", "foo", "bar"), Equals, prefix+"/people/123?foo=bar")
			c.Expect(app.Url("People", "Index"), Equals, prefix+"/people")
			c.Expect(app.Url("People", "Index", "id", "123"), Equals, prefix+"/people?id=123")

			u, _ := app.URL("People", "Update", "id", "123")
			c.Expect(u, IsNil)
			c.Expect(app.ModelUrl(&Person{"122"}), Equals, prefix+"/people/122")

		})

		c.Specify("Controller's Init() should be called", func() {
			m := Mice{}
			app.AddController(&m)
			c.Expect(m.initCalled, Equals, 1)
			c.Expect(m.ApplicationController.initCalled, Equals, 1)
		})

		c.Specify("Should handle non-rest routes", func() {
			app.AddController(&Mice{})
			c.Expect(len(app.routes), Equals, 5) // 4 custom + 1 REST

			prefix := app.RoutePrefix

			c.Expect(app.Url("Mice", "Show", "id", "345"), Equals, prefix+"/mice/345")
			c.Expect(app.Url("Mice", "Foo", "id", "123"), Equals, prefix+"/mice/foo?id=123")
			c.Expect(app.Url("Mice", "Bar", "id", "123"), Equals, prefix+"/mice/bar/123")
			c.Expect(app.Url("Mice", "Baz", "year", "2013", "month", "5"), Equals, prefix+"/mice/baz/2013/5")

			u, _ := app.URL("Mice", "Index")
			c.Expect(u, IsNil)

			u, _ = app.URL("Mice", "Display")
			c.Expect(u, IsNil)

			c.Expect(app.ModelUrl(&Mouse{"122"}), Equals, prefix+"/mice/122")
		})

		c.Specify("Should handle route prefix", func() {
			app.AddController(&People{})
			app.SetDefaultRoute("People", "Index")

			c.Assume(app.RoutePrefix, Equals, "/app")
			tests := map[string]string{
				"/app/":       "200 People Index",
				"/app":        "301",
				"/":           "404",
				"/people/999": "404",
			}
			for uri, expect := range tests {
				withBody := false
				if strings.Contains(expect, " ") {
					withBody = true
				}
				r := testHttp(app, uri, withBody)
				c.Expect(r, Equals, fmt.Sprintf("%s %s", uri, expect))
			}
		})

		c.Specify("Should call Before() and After() filters on every request", func() {
			m := Mice{}
			app.AddController(&m)
			c.Expect(testHttp(app, "mice/bar/foo", true), Equals, "mice/bar/foo 200 Bar id=0")

			c.Expect(m.beforeCalled, Equals, 1)
			c.Expect(m.ApplicationController.beforeCalled, Equals, 1)
			c.Expect(m.afterCalled, Equals, 1)
			c.Expect(m.ApplicationController.afterCalled, Equals, 1)
		})

		c.Specify("Should take response from Before()", func() {
			p := People{}
			app.AddController(&p)
			c.Expect(testHttp(app, "people/1121/edit", true), Equals, `people/1121/edit 302 <a href="/app/login">Found</a>.`)
		})

		c.Specify("Should call controller methods", func() {
			app.AddController(&Mice{})
			app.AddController(&People{})
			app.SetDefaultRoute("People", "Index")

			tests := map[string]string{
				"people":         "200 People Index",
				"people/":        "200 People Index",
				"people/999":     "200 Person id=999",
				"people/foo":     "200 Person id=foo",
				"mice":           "404",
				"mice/1234":      "200 Mouse id=1234",
				"mice/foo":       "200 Foo",
				"mice/bar/12345": "200 Bar id=12345",
			}
			for uri, expect := range tests {
				withBody := false
				if strings.Contains(expect, " ") {
					withBody = true
				}
				r := testHttp(app, uri, withBody)
				c.Expect(r, Equals, fmt.Sprintf("%s %s", uri, expect))
			}
		})

		c.Specify("Should pass 0 for invalid integer params", func() {
			app.AddController(&Mice{})
			c.Expect(testHttp(app, "mice/bar/foo", true), Equals, "mice/bar/foo 200 Bar id=0")
		})

		c.Specify("Should recover from panics with status 500", func() {
			app.AddController(&Mice{})
			c.Expect(testHttp(app, "mice/baz/2013/05", false), Equals, "mice/baz/2013/05 500")
			c.Expect(testHttp(app, "mice/panic", false), Equals, "mice/panic 500")
		})

		c.Specify("Should render templates", func() {
			al := &Aliases{}
			app.AddController(al)

			createTestTemplate(app.ViewsPath+"/aliases/show.html", "Fish {{.alias.Id}}")
			uri := "/app/aliases/1024"
			c.Expect(testHttp(app, uri, true), Equals, uri+" 200 Fish 1024")
		})

		c.Specify("Should render templates with layout", func() {
			al := &Aliases{}
			app.AddController(al)
			al.SetLayout("blank")

			os.Mkdir(app.ViewsPath+"/layout", 0755)
			if err := ioutil.WriteFile(app.ViewsPath+"/layout/blank.html", []byte(`[{{ template "content" . }}]`), 0644); err != nil {
				panic(err)
			}

			createTestTemplate(app.ViewsPath+"/aliases/show.html", `{{ define "content" }}Fish {{.alias.Id}}{{ end }}`)
			uri := "/app/aliases/1024"
			c.Expect(testHttp(app, uri, true), Equals, uri+" 200 [Fish 1024]")
		})

		c.Specify("Should save sessions", func() {
			app.AddController(&Mice{})
			c.Assume(app.RoutePrefix, Equals, "/app")

			r, _ := http.NewRequest("GET", "/app/mice/1234", nil)
			rw := httptest.NewRecorder()
			//rw.Body = new(bytes.Buffer)
			app.ServeHTTP(rw, r)

			cookie, err := parseCookie(rw.Header().Get("Set-Cookie"))
			if err != nil {
				panic(err)
			}

			r, _ = http.NewRequest("GET", "/app/mice/1234", nil)
			r.AddCookie(cookie)

			req := app.newRequest(r, "Mice", "Index", "")
			c.Expect(req.Session.Values["id"], Equals, 1234)
			c.Expect(req.Session.Values["testValue"], Equals, 2*1234)
		})
	})
}
