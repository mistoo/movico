package movico

import (
	"fmt"
	"net"
	"net/http"
	"os"
	"path"

	"github.com/daaku/go.grace"
)

func (app *Application) openPidFile() (*os.File, error) {
	u := fmt.Sprintf("%s.%s.pid", app.config["listen"], app.config["port"])
	ppath := path.Join(app.RootPath, "log", u)
	file, err := os.OpenFile(ppath, os.O_RDWR|os.O_CREATE, 0600)

	if err != nil {
		return nil, fmt.Errorf("Failed to open pid file: %v", err)
	}
	return file, nil
}

func (app *Application) writePid() error {
	file, err := app.openPidFile()
	if err != nil {
		return err
	}

	app.pidfile = file
	app.Log.Trace("Writing pid %d to %s", os.Getpid(), app.pidfile.Name())
	//if err := syscall.Flock(int(app.pidfile.Fd()), syscall.LOCK_EX | syscall.LOCK_NB); err != nil {
	//	return err
	//}
	_, err = app.pidfile.WriteString(fmt.Sprintf("%d", os.Getpid()))
	return err
}

/*
func (app *Application) unlockPid() {
	//file.WriteString(fmt.Sprintf("%d", os.Getpid()))
	//app.Log.Trace("Locking %s %d", file.Name(), int(file.Fd()))
	if app.pidfile != nil {
		app.Log.Trace("Unlocking %d %s", os.Getpid(), app.pidfile.Name())
		syscall.Flock(int(app.pidfile.Fd()), syscall.LOCK_UN)
		app.pidfile.Close()
		app.pidfile = nil
	}
}
*/

func (app *Application) simpleRun() {
	app.setupStaticHandler()
	l, err := app.newListener()
	if err != nil {
		app.Log.Error(err.Error())
	}
	http.Serve(l, app)
}

// Stops web server.
func (app *Application) simpleStop() {
	app.listener.Close()
	app.Log.Info("Server stopped")
}

func (app *Application) Run() error {
	app.setupStaticHandler()
	err := app.graceServe()
	if err != nil {
		app.Log.Error(err.Error())
		return err
	}
	return nil
}

func (app *Application) newListener() (grace.Listener, error) {
	u := fmt.Sprintf("%s:%s", app.config["listen"], app.config["port"])
	addr, err := net.ResolveTCPAddr("tcp", u)
	if err != nil {
		return nil, err
	}

	l, err := net.ListenTCP("tcp", addr)
	if err != nil {
		return nil, err
	}
	app.listener = l

	return grace.NewListener(l), nil
}

func (app *Application) graceServe() error {
	var listener grace.Listener

	listeners, err := grace.Inherit()
	if err == grace.ErrNotInheriting { // new
		listener, err = app.newListener()
		if err != nil {
			return err
		}
		if err = app.writePid(); err != nil {
			return err
		}
		app.Log.Info("Listen %s with pid %d", listener.Addr(), os.Getpid())

	} else if err == nil { // inherited
		listener = listeners[0]
		err = grace.CloseParent()
		if err != nil {
			return fmt.Errorf("Failed to close parent: %s", err)
		}
		ppid := os.Getppid()
		if ppid == 1 {
			app.Log.Info("Listening on init activated %s", listener.Addr())
		} else {
			app.Log.Info("Graceful handoff of %s with new pid %d and old pid %d.",
				listener.Addr(), os.Getpid(), ppid)
		}
		app.listener = listener
		if err = app.writePid(); err != nil {
			return err
		}

	} else {
		return fmt.Errorf("Failed graceful handoff: %s", err)
	}

	err = app.graceServeWait(listener)
	if err != nil {
		return err
	}
	app.Log.Info("Exiting pid %d.", os.Getpid())
	return nil
}

func (app *Application) graceServeWait(listener grace.Listener) error {
	errch := make(chan error, 2) // listener + grace.Wait
	go func(l net.Listener) {
		err := http.Serve(l, app)
		// The underlying Accept() will return grace.ErrAlreadyClosed
		// when a signal to do the same is returned, which we are okay with.
		if err != nil && err != grace.ErrAlreadyClosed {
			errch <- fmt.Errorf("Failed http.Serve: %s", err)
		}
	}(listener)

	go func() {
		err := grace.Wait([]grace.Listener{listener})
		if err != nil {
			errch <- fmt.Errorf("Failed grace.Wait: %s", err)
		} else {
			errch <- nil
		}
	}()
	return <-errch
}
