package movico

import (
	"fmt"
	"path"
	"path/filepath"
	"strings"

	"bitbucket.org/pkg/inflect"
)

// 
type ControllerInterface interface {
	Name() string // BarsController
	RoutePath() string // /bars
	SetRoutePath(string)
	AddRoute(method string, path string) *Route // adds a new route to method
	RouteFor(method string) *Route						// returns existing route for method
	controllerInstance() *Controller
}

// Controller type 
type Controller struct {
	App *Application // Application reference
	Log *Logger	 // Logger reference

	className string // eg. HappyPeople
	modelName string // eg. HappyPerson
	viewsPath string // eg. happy_people
	routePath string // eg. /happy-people

	layoutPath string

	formats     []string
	restRoutes  map[string]*Route
	routes      map[string]*Route
	initialized bool
}

func (c *Controller) controllerInstance() *Controller { return c }

// Returns controller name 
func (c *Controller) Name() string               { return c.className }

// Controller route path, /<controller-name> by default
func (c *Controller) RoutePath() string               { return c.routePath }

// Sets controller route path 
func (c *Controller) SetRoutePath(routePath string) {
	c.routePath = path.Clean(path.Join("/", routePath))
}

// Controller layout template base path, "application" by default
func (c *Controller) Layout() string { return filepath.Base(c.layoutPath) }

// Sets non-default layout
func (c *Controller) SetLayout(layout string) {
	if layout == "" {
		c.layoutPath = ""
	} else {
		c.layoutPath = c.layoutTemplatePath(layout)
	}
}

func (c *Controller) methodTemplatePath(method string) string {
	return path.Join(c.viewsPath, strings.ToLower(method))
}

func (c *Controller) templatePath(templatePath string) string {
	return path.Join(c.App.ViewsPath, templatePath) + c.App.TemplateEngine.Ext()
}

func (c *Controller) layoutTemplatePath(layout string) string {
	return path.Join(c.App.ViewsPath, "layout/"+layout+c.App.TemplateEngine.Ext())
}

// ModelUrl() returns model route e.g. ModelUrl(&Post{Id: 1}) => <controller-route-path>/posts/1.
// It is wrapper for Application.ModelUrl()
func (c *Controller) ModelUrl(model Modeler) string {
	return c.App.ModelUrl(model)
}

// Url for given method e.g. c.MethodUrl("Show", "id", "1") => <controller-route-path>/show/1
func (c *Controller) MethodUrl(method string, pairs ...string) string {
	return c.App.Url(c.Name(), method, pairs...)
}

func (c *Controller) initializeController(className string, app *Application) {
	if c.initialized == true {
		panic(fmt.Sprintf("%s already initialized", className))
	}

	c.className = className
	name := strings.Replace(c.className, "Controller", "", -1)
	t := inflect.Tableize(name)
	c.viewsPath = t
	ts := inflect.Singularize(t)
	c.modelName = inflect.Camelize(ts)
	c.routePath = "/" + strings.Replace(t, "_", "-", -1)

	c.formats = []string{"html"}
	c.restRoutes = map[string]*Route{}

	c.addRestRoute("Index", "/", "GET").Name(t)
	c.addRestRoute("Show", "/{id}", "GET").Name(ts)
	c.addRestRoute("New", "/new", "GET").Name("new_" + ts)
	c.addRestRoute("Create", "/", "POST").Name("create_" + ts)
	c.addRestRoute("Edit", "/{id}/edit", "GET").Name("edit_" + ts)
	c.addRestRoute("Update", "/{id}", "PUT").Name("update_" + ts)
	c.addRestRoute("Delete", "/{id}", "DELETE").Name("delete_" + ts)

	c.routes = make(map[string]*Route)
	c.App = app
	c.Log = app.Log
	c.SetLayout("application")
	c.initialized = true
}

func (c *Controller) addRestRoute(method string, path string, httpMethod string) *Route {
	route := newRoute(c.className, method, path).Formats(c.formats...).Method(httpMethod)
	c.restRoutes[method] = route
	return route
}

// Adds route for given controller method; path argument is 
// e.g. AddRoute("Display", "/display/{id}")
func (c *Controller) AddRoute(method string, path string) *Route {
	route := newRoute(c.className, method, path).Formats(c.formats...)
	c.routes[method] = route
	return route
}

// Set default formats controller support. Default format is "html". 
func (c *Controller) SetDefaultFormats(formats ...string) error {
	if len(formats) == 0 {
		return fmt.Errorf("%s.DefaultFormats: formats cannot be blank", c.Name())
	}

	for _, r := range c.routes {
		r.Formats(formats...)
	}

	for _, r := range c.restRoutes {
		r.Formats(formats...)
	}

	c.formats = formats
	return nil
}

// Returns existing route for given method
func (c *Controller) RouteFor(method string) *Route {
	r, ok := c.routes[method]
	if ok == false {
		r, ok = c.restRoutes[method]
	}
	if ok == false {
		return nil
	}

	if r.formats == nil {
		r.Formats(c.formats...)
	}

	return r
}

// Renders to response given template within layout 
func (c *Controller) RenderTemplateTo(re *Response, layout, template string, pairs ...interface{}) *Response {
	if layout == "" {
		layout = c.layoutPath
	} else {
		layout = c.layoutTemplatePath(layout)
	}

	if template == "" {
		template = c.templatePath(re.req.TemplatePath)
	} else {
		template = c.templatePath(template)
	}

	renderer, err := c.App.newRenderer(re.req)
	if err != nil {
		panic(fmt.Errorf("%s: renderer initialization error: %s", re.req.MethodFQN, err))
	}
	renderer.SetLayout(layout)
	renderer.SetTemplate(template)
	renderer.Render(re, pairs...)
	return re
}

// Renders given template within controller layout. Returns new Response.
func (c *Controller) RenderTemplate(req *Request, template string, pairs ...interface{}) *Response {
	re := req.Response()
	return c.RenderTemplateTo(re, "", template, pairs...)
}

// Renders to response given template within layout. Returns new Response. 
func (c *Controller) RenderLayoutTemplate(req *Request, layout, template string, pairs ...interface{}) *Response {
	re := req.Response()
	return c.RenderTemplateTo(re, layout, template, pairs...)
}

/*
func (c *Controller) renderToAs(re *Response, ma Marshaler, pairs ...interface{}) *Response {
	if len(pairs) < 2 {
		return re.Error500(fmt.Errorf("%s.renderToAs(): nothing to render", c.Name()))
	}

	if len(pairs) != 2 {	// not a simple Render("object", object)
		return re.Error500(fmt.Errorf("%s.renderToAs(): too much to marshal, implement your own marshal", c.Name()))
	}

	if _, ok := pairs[0].(string); !ok {
		return re.Error500(fmt.Errorf("%s.renderToAs(): first arg must be a string", c.Name()))
	}

	mmessage, err := ma.Marshal(pairs[1])
	if err != nil {
		return re.Error500(err)
	}
	re.Write(mmessage)
	return re
}*/

// Render to response default template 
func (c *Controller) RenderTo(re *Response, pairs ...interface{}) *Response {
	if len(pairs) % 2 != 0 {
		re.Error500(fmt.Errorf("Odd number of pairs arguments (%d) to %s.RenderTo()", len(pairs), c.Name()))
	}

	return c.RenderTemplateTo(re, "", "", pairs...)
}

// Render default template to new Response
func (c *Controller) Render(req *Request, pairs ...interface{}) *Response {
	re := req.Response()
	return c.RenderTo(re, pairs...)
}

// Decodes schema using gorilla toolkit schema package
func (c *Controller) DecodeSchema(req *Request, model Modeler) error {
	p := req.ModelParams(model)
	err := c.App.SchemaDecoder.Decode(model, p)
	c.Log.Trace("XDecodeSchema %+v from %+v (err = %v)", model, p, err)
	return err
	
}


// Decodes schema using gorilla toolkit schema package
func (c *Controller) EncodeSchema(model Modeler) (map[string][]string, error) {
	//c.Log.Trace("DecodeSchema %+v from %+v", model, p)
	return c.App.SchemaDecoder.Encode(model)
}
